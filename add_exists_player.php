<?php 
include_once('session_check.php'); 
include_once('connect.php');
if (isset($_REQUEST["gameid"])) {

    $gameid = $_REQUEST["gameid"];
    $team_code = $_REQUEST["teamid"];
    $team_type = $_REQUEST["teamtype"];
    $homeQry = "SELECT * FROM individual_player_stats where gamecode = ".$gameid." and teamcode = ".$team_code;
    $home_data = $conn->prepare($homeQry);
    $home_data->execute();
    $get_home_rowCount = $home_data->rowCount();
    if ($get_home_rowCount > 0) {
        $fetch_Hmedata = $home_data->fetchAll(PDO::FETCH_ASSOC);
        $HomeTeamPlayersArr = "";
        foreach ($fetch_Hmedata as $fetch_HomeTeamRow) {
            $HomeTeamPlayersArr[] = $fetch_HomeTeamRow['playercode'];
        }                    
    } else {
        $homeTeamQry = "SELECT * FROM player_info where team_id = ".$team_code;
        $home_team_data = $conn->prepare($homeTeamQry);
        $home_team_data->execute();
        $get_home_team_rowCount = $home_team_data->rowCount();
        if ($get_home_team_rowCount > 0) {
            $fetch_HmeTeamdata = $home_team_data->fetchAll(PDO::FETCH_ASSOC);
            $HomeTeamPlayersArr = "";
            foreach ($fetch_HmeTeamdata as $fetch_HomeTeamRow) {
                $HomeTeamPlayersArr[] = $fetch_HomeTeamRow['id'];
            }
        }
    }

    $ImHomeTeamPlayers = implode(",", $HomeTeamPlayersArr);
    $playerDropQry = "SELECT * FROM player_info where team_id=$team_code and id not in ($ImHomeTeamPlayers)";
    $playerDropQry = $conn->prepare($playerDropQry);
    $playerDropQry->execute();
    $playerRowCount = $playerDropQry->rowCount();
    $DiffplayersArr = "";
    if ($playerRowCount > 0) {
        $fetch_HmeTeamPlayers = $playerDropQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($fetch_HmeTeamPlayers as $fetch_HomeTeamRow) {
            $DiffplayersArr[] = $fetch_HomeTeamRow;
        }
    } else {
        $DiffplayersArr = array();
    }
}
?>
<div class="modal-dialog" style="text-align:left;">
    <input type="hidden" id="assigngameid" value="<?php echo $gameid;?>">
    <input type="hidden" id="assignteamid" value="<?php echo $team_code;?>">
    <input type="hidden" id="assignteamtype" value="<?php echo $team_type;?>">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign player</h4>
        </div>
        <div class="modal-body assign-player-modal-body">
            <div class="col-md-12 col-sm-12 col-xs-12 alertmessage">
            </div>
            <table class="table addplayertable table-bordered table-hover">
                <tr>
                    <th class="selectplayer" >
                        <div class="form-group checkallstyle">
                            <div class="" style="padding-top:0px;">
                                <label class="mt-checkbox mt-checkbox-outline" >
                                    <input type="checkbox" value="1" id="checkAll">
                                    <span></span>
                                </label>                                
                            </div>
                        </div>
                    </th>
                    <th>Playercode</th>
                    <th>Playerimage</th>
                    <th>Playername</th>
                </tr>
                <?php 
                if (count($DiffplayersArr) > 0) {
                    foreach ($DiffplayersArr as $DiffplayersArrRow) { ?>
                    <tr>                    
                        <td class="selectplayer">
                            <div class="form-group checkallstyle">
                                <div class="" style="padding-top:0px;">
                                    <label class="mt-checkbox mt-checkbox-outline" >
                                        <input type="checkbox" class="playercheckbox" name="playercheckbox[]" value="<?php echo $DiffplayersArrRow["id"]; ?>">
                                        <span></span>
                                    </label>                                
                                </div>
                            </div>
                        </td>
                        <td><?php echo $DiffplayersArrRow["id"]; ?></td>
                        <?php $playerImage = "uploads/players/".$DiffplayersArrRow["image"]; 
                        // $playerImage = "uploads/players/sample.png";
                        ?>
                        <td><img src="<?php echo $playerImage; ?>" style="width:40px;height:40px;"></td>
                        <td><?php echo $DiffplayersArrRow["lastname"].", ".$DiffplayersArrRow["firstname"]; ?></td>
                        
                    </tr>
                <?php } } else {?>
                <tr><td colspan="4" style="padding-top:7px;padding-bottom:7px;text-transform: capitalize;">No player(s) found</td></tr>
                <?php } ?>
            </table>            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn customgreenbtn" id="continuebtn">Continue</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script src="assets/custom/js/gamestats.js" type="text/javascript"></script>
<script>
    $( document ).ready(function() {
        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));

        });

        // $(document).on("click",'#continuebtn', function() {
        $("#continuebtn").click(function () {
            var values = new Array();

            $.each($("input[name='playercheckbox[]']:checked"), function() {
                values.push($(this).val());
                                
            });
            if (values.length > 0) {

                var myJsonString = JSON.stringify(values);
                var assigngameid = $("#assigngameid").val();
                var assignteamid = $("#assignteamid").val();
                var assignteamtype = $("#assignteamtype").val();

                $.ajax({
                    url:"assign_player.php",
                    method: "GET",
                    cache:false,
                    data: {assignplayer:myJsonString, gameid: assigngameid, teamid: assignteamid, assignteamtype: assignteamtype},
                    success:function(result){
                        // console.log(result);
                        
                        var myArray = JSON.parse(result);
                        for (var i=0;i<myArray.length;i++) {
                            var player_id = myArray[i]["playercode"];
                            var playername = myArray[i]["playername"];
                            if (myArray[0]["status"] == "success") { 
                                var visitorPlayerTableHTML = '<tr><input type="hidden" name="visitor_id[]" value="'+player_id+'"><td nowrap >'+playername+'</td><td><input type="text" style="width:35px;" class="visitor_gp" data-val="int" name="visitor_gp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_gs" data-val="int" name="visitor_gs['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptmade" data-val="int" name="visitor_2ptmade['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptatt" data-val="int" name="visitor_2ptatt['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fgm" data-val="int" name="visitor_3fgm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fga" data-val="int" name="visitor_3fga['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ftm"data-val="int" name="visitor_ftm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_fta" data-val="int" name="visitor_fta['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tp" data-val="int" name="visitor_tp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_oreb" data-val="int" name="visitor_oreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_dreb" data-val="int" name="visitor_dreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_treb" data-val="int" name="visitor_treb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_pf" data-val="int" name="visitor_pf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tf" data-val="int" name="visitor_tf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ast" data-val="int" name="visitor_ast['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_to" data-val="int" name="visitor_to['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_blk" data-val="int" name="visitor_blk['+player_id+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="visitor_stl" data-val="int" name="visitor_stl['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_min" data-val="int" name="visitor_min['+player_id+']" value=""></td></tr>';
                                var homePlayerTableHTML = '<tr><input type="hidden" name="home_id[]" value="'+player_id+'"><td nowrap >'+playername+'</td><td><input type="text" style="width:35px;" class="home_gp" data-val="int" name="home_gp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_gs" data-val="int" name="home_gs['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptmade" data-val="int" name="home_2ptmade['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptatt" data-val="int" name="home_2ptatt['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fgm" data-val="int" name="home_3fgm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fga" data-val="int" name="home_3fga['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_ftm"data-val="int" name="home_ftm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_fta" data-val="int" name="home_fta['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_tp" data-val="int" name="home_tp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_oreb" data-val="int" name="home_oreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_dreb" data-val="int" name="home_dreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_treb" data-val="int" name="home_treb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_pf" data-val="int" name="home_pf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_tf" data-val="int" name="home_tf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_ast" data-val="int" name="home_ast['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_to" data-val="int" name="home_to['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_blk" data-val="int" name="home_blk['+player_id+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="home_stl" data-val="int" name="home_stl['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_min" data-val="int" name="home_min['+player_id+']" value=""></td></tr>';
                                if (myArray[0]["teamtype"] == "visitorteam")
                                    $("#search_result").find('tr:last').prev().after(visitorPlayerTableHTML);
                                else
                                    $("#search_result_home").find('tr:last').prev().after(homePlayerTableHTML);
                            }
                        }
                        $('#addExistsPlayerModal').modal('hide');
                    }
                });
            } else {
                var numPlayers = $('.playercheckbox').length;
                var AlertHtml = '<div class="alert alert-danger fade in message"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>Please select atleast one player</div>';
                if (numPlayers > 0) {
                    $(".alertmessage").empty().append(AlertHtml);
                }
            }    
        });

        
    });


</script>

