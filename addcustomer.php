<?php 
include_once('session_check.php'); 
include_once("connect.php");

if (!empty($_SESSION['loginid']))  {

    if ($_SESSION['usertype'] == 'admin') {
        $uid = $_SESSION['loginid']; 
    } else { 
        header('Location:login.php');
        exit;
    }
} else {
    header('Location:login.php');
    exit;
}

if (isset($_GET['id'])) {

    $id = base64_decode($_GET['id']);
    $custid = base64_decode($_GET['id']);

    $custqry = $conn->prepare("select * from customer_info where id=:id");
    $QryArr = array(":id"=>$id);        
    $custqry->execute($QryArr);
    $row = $custqry->fetch(PDO::FETCH_ASSOC);   

    $idDB = $row['id'];
    $NameDB = $row['name'];
    $AbbrDB = $row['abbrevation'];
    $AddressDB = $row['address'];
    $CityDB = $row['city'];
    $StateDB = $row['state'];
    $ZipDB = $row['zip'];
    $SitUrlDB = $row['site_url'];
    $CustTypeDB = $row['customer_type'];
    $ScoringPlatFormDB = $row['scoring_platform'];
    $SitTempDB = $row['site_template'];
    $LiveScoringTemDB = $row['livescoring_template'];
    $PasswordDB = $row['password'];
    $VideoDB = $row['video'];
    $ConferenceDB = $row['conference'];
    $AddInfoDB = $row['additional_info'];
    $ConNameDB = $row['contact_name'];
    $ConPosDB = $row['contact_position'];
    $ConAddDB = $row['contact_address'];
    $ConCityDB = $row['contact_city'];
    $ConStateDB = $row['contact_state'];
    $ConZipDB = $row['contact_zip'];
    $ConPhoneDB = $row['contact_phone'];
    $ConEmailDB = $row['contact_email'];
    $PaySchedDB = $row['payment_schedule'];
    $RenewalDB = $row['renewal_date'];
    $BgImgDB = $row['bgimg'];
    $AdvDB = $row['adv'];
    $GaDB = $row['GA']; 
    $mode = "edit";

} else {
    $id = "";
    $custid = "";
    $idDB = "";
    $NameDB = "";
    $AbbrDB = "";
    $AddressDB = "";
    $CityDB = "";
    $StateDB = "";
    $ZipDB = "";
    $SitUrlDB = "";
    $CustTypeDB = "";
    $ScoringPlatFormDB = "";
    $SitTempDB = "";
    $LiveScoringTemDB = "";
    $PasswordDB = "";
    $VideoDB = "";
    $ConferenceDB = "";
    $AddInfoDB = "";
    $ConNameDB = "";
    $ConPosDB = "";
    $ConAddDB = "";
    $ConCityDB = "";
    $ConStateDB = "";
    $ConZipDB = "";
    $ConPhoneDB = "";
    $ConEmailDB = "";
    $PaySchedDB = "";
    $RenewalDB = "";
    $BgImgDB = "";
    $AdvDB = "";
    $GaDB = "";
    $mode = "add";
}

if (isset($_POST['addsubmit'])) {
    
    $OrganizationName = strip_tags($_POST['OrganizationName']);
    $Nickname = strip_tags($_POST['Nickname']);
    $Address = strip_tags($_POST['Address']);
    $City = strip_tags($_POST['City']);
    $State = strip_tags($_POST['State']);
    $Zip = strip_tags($_POST['Zip']);
    $OrgType = isset($_POST['OrgType']) ?$_POST['OrgType'] : "";
    $weburl = strip_tags($_POST['weburl']);
    $Scoring = $_POST['Scoring'];
    $password = $_POST['password'];
    $addinfo = strip_tags($_POST['addinfo']);
    $contact_name = strip_tags($_POST['contact_name']);
    $contact_position = strip_tags($_POST['contact_position']);
    $contact_address = strip_tags($_POST['contact_address']);
    $contact_city = strip_tags($_POST['contact_city']);
    $contact_state = strip_tags($_POST['contact_state']);
    $contact_zip = strip_tags($_POST['contact_zip']);
    $contact_phone = strip_tags($_POST['contact_phone']);
    $email = strip_tags($_POST['email']);
    $sitetemp = strip_tags($_POST['sitetemp']);
    $conference = $_POST['conference'];
    $video = (int)$_POST['video'];
    if ($video == 1) {
        $streams = strip_tags($_POST['streams']);
        $bandwidth = strip_tags($_POST['bandwidth']);
        $storage = strip_tags($_POST['storage']);
        $rate = strip_tags($_POST['rate']);
        $sdate = date('d/m/Y');
    }

    if ($sitetemp == "") {
        $sitetemp = $_POST['sitetempreq'];
    }
    $payschedule = $_POST['payschedule'];
    $livescoring = $_POST['livescoring'];
    if (empty($_POST['newrenewdate'])) {        
        $strtotimedate = !empty($_POST['renewdate']) ? strtotime($_POST['renewdate']) : "" ;
        $renewdate = date("m/d/Y", $strtotimedate);
    } else {
       
        $strtotimedate = !empty($_POST['renewdate']) ? strtotime($_POST['newrenewdate']) : "" ;
        $renewdate = date("m/d/Y", $strtotimedate); 
    }
    
    $ss = $_POST['sport_list'];
    $s = $ss[0];

    $uname = explode(".",$weburl);
    $username = $uname[0];
}
if ($mode == "add") {
    if (isset($_POST['addsubmit'])) {

        /* subdomain and ftp creation */

        // include('xmlapi.php');
        // $xmlapi = new xmlapi('805stats.com');
        // $xmlapi->password_auth('stats', 'Kepe2069');
        // $xmlapi->set_debug(1);

        // $xmlapi->api1_query('805stats', 'SubDomain', 'addsubdomain', array($username, '805stats.com', 0, 0, '/public_html/customers/'.$username));

        // if($OrgType!=5)
        // {
        // $xmlaccess="public_html/customers/".$username."/livescoring/xml";
        // }
        // else
        // {
        // $xmlaccess="public_html/customers/".$username."/xml";

        // }
        // $xmlapi->api1_query('805stats', 'Ftp', 'addftp', array($username, $password,$xmlaccess, '20')); 

        // $databasename=$username;
        // $databaseuser="admin";
        // $databasepass="Karol@0826";
        // $cpaneluser="stats";

        // if($sport=='4441' || $sport=='4442')
        // {}
        // else
        // {
        // // $createdb = $xmlapi->api1_query($cpaneluser, "Mysql", "adddb", array($databasename));   

        // // $addusr = $xmlapi->api1_query($cpaneluser, "Mysql", "adduserdb", array("".$cpaneluser."_".$databasename."", "".$cpaneluser."_".$databaseuser."", 'all')); 
        // }
        // if($OrgType!=5)
        // { 
        // $dst="/home/stats/public_html/customers/".$username."/";

        // $dst1="/home/stats/public_html/customers/".$username."/livescoring/";
        // $src="/home/stats/public_html/panel/FILES/templates/".$sitetemp."/";  


        // $src1="/home/stats/public_html/panel/FILES/livescoring_screens/website_customer/".$Scoring."/".$livescoring."/";



        // function recurse_copy($src,$dst) {
        //   $dir = opendir($src); 
        //   @mkdir($dst); 
        //   while(false !== ( $file = readdir($dir)) ) { 
        //       if (( $file != '.' ) && ( $file != '..' )) { 
        //          if ( is_dir($src . '/' . $file) ) { 
        //              recurse_copy($src . '/' . $file,$dst . '/' . $file); 
        //          } 
        //          else { 
        //              copy($src . '/' . $file,$dst . '/' . $file); 
        //          } 
        //       } 
        //   } 
        //  closedir($dir); 
        // }

        // recurse_copy($src,$dst);

        // recurse_copy($src1,$dst1);
        // }
        // else
        // {
        // $dst1="/home/stats/public_html/customers/".$username."/";
        // $src1="/home/stats/public_html/panel/FILES/livescoring_screens/livescore_customer/".$Scoring."/".$s."/".$livescoring."/";



        // function recurse_copy($src,$dst) {
        //   $dir = opendir($src); 
        //   @mkdir($dst); 
        //   while(false !== ( $file = readdir($dir)) ) { 
        //       if (( $file != '.' ) && ( $file != '..' )) { 
        //          if ( is_dir($src . '/' . $file) ) { 
        //              recurse_copy($src . '/' . $file,$dst . '/' . $file); 
        //          } 
        //          else { 
        //              copy($src . '/' . $file,$dst . '/' . $file); 
        //          } 
        //       } 
        //   } 
        //  closedir($dir); 
        // }

        // recurse_copy($src1,$dst1);
        // }
        // $headers  = 'MIME-Version: 1.0' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        // $headers .= 'From: support@805stats.com';
        // $message="New user registered. FTP:".$username."@805stats.com <br/>Password :".$password."<br/>Email:".$email;
        // mail('support@805stats.com','New user registered', $message, $headers);


        // $insert_custinfo_qry = $conn->prepare("INSERT INTO customer_info(name, abbrevation, address, city, state, zip, customer_type, site_url, scoring_platform, password, additional_info, contact_name, contact_position, contact_address, contact_city, contact_state, contact_zip, contact_phone, contact_email, site_template, payment_schedule, livescoring_template, renewal_date, user_type, email,conference,video) VALUES (:name, :abbrevation, :address, :city, :state, :zip, :customer_type, :site_url, :scoring_platform, :password, :additional_info, :contact_name, :contact_position, :contact_address, :contact_city, :contact_state, :contact_zip, :contact_phone, :contact_email, :site_template, :payschedule, :livescoring_template, :renewal_date, :user_type, :email,:conference,:video)");

        $insertcust = $conn->prepare("INSERT INTO customer_info(name, abbrevation, address, city, state, zip, customer_type, site_url, scoring_platform, password, additional_info, contact_name, contact_position, contact_address, contact_city, contact_state, contact_zip, contact_phone, contact_email, site_template, payment_schedule, livescoring_template, renewal_date, user_type, email,conference,video) VALUES ('$OrganizationName', '$Nickname', '$Address', '$City', '$State', '$Zip', '$OrgType', '$weburl', '$Scoring', '$password', '$addinfo', '$contact_name', '$contact_position', '$contact_address', '$contact_city', '$contact_state', '$contact_zip', '$contact_phone', '$email', '$sitetemp', '$payschedule', '$livescoring', '$renewdate', 'user', '$email','$conference','$video')");
       

        $QryArr = array(":name"=>$OrganizationName, ":abbrevation"=>$Nickname, ":address"=>$Address, ":city"=>$City, ":state"=>$State, ":zip"=>$Zip, ":customer_type"=>$OrgType, ":site_url"=>$weburl, ":scoring_platform"=>$Scoring,":password"=>$password, ":additional_info"=>$addinfo, ":contact_name"=>$contact_name, ":contact_position"=>$contact_position, ":contact_address"=>$contact_address, ":contact_city"=>$contact_city, ":contact_state"=>$contact_state, ":contact_zip"=>$contact_zip, ":contact_phone"=>$contact_phone,":contact_email"=>$email, ":site_template"=>$sitetemp, ":payschedule"=>$payschedule,":livescoring_template"=>$livescoring, ":renewal_date"=>$renewdate, ":user_type"=>"user", ":email"=>$email, ":conference"=>$conference, ":video"=>$video);

        // $insert_custinfo_qry->execute($QryArr);
        $insertcust->execute();
        $custid = $conn->lastInsertId();

        if ($video == 1) {
            // $insert_custvideo_qry = $conn->prepare("INSERT INTO customer_video(customer_id, sdate, streams, bandwidth, storage, rate) VALUES (:custid, :sdate, :streams, :bandwidth, :storage, :rate)");
            // echo "INSERT INTO customer_video(customer_id, sdate, streams, bandwidth, storage, rate) VALUES ('$custid', '$sdate', '$streams', '$bandwidth', '$storage', '$rate')";exit;
            $insert_custvideo_qry = $conn->prepare("INSERT INTO customer_video(customer_id, sdate, streams, bandwidth, storage, rate) VALUES ('$custid', '$sdate', '$streams', '$bandwidth', '$storage', '$rate')");
            $QryArr = array(":custid"=>$custid, ":sdate"=>$sdate, ":streams"=>$streams, ":bandwidth"=>$bandwidth, ":storage"=>$storage, ":rate"=>$rate);
            $insert_custvideo_qry->execute($QryArr);
        }

        $timestamp = mktime(date("h"), date("i"), date("s"), date("m"), date("d"), date("Y"));

        if ($OrgType==5) {
            $xmlnames = '';
            for($i=0;$i<count($_POST['sport_list']);$i++){  
                $xmlnames .= $Scoring."_".$custid."_".$_POST['sport_list'][$i]."_".$timestamp.".xml";
                }
            $update_custinfo_qry = $conn->prepare("update customer_info set livescore_xml_name=:livescore_xml_name where id=:custid");      
            $QryArr = array(":livescore_xml_name"=>$xmlnames, ":custid"=>$custid);        
            $update_custinfo_qry->execute($QryArr); 

        }

        if (!empty($_POST['sport_list'])) {

            $checked_count = count($_POST['sport_list']);
            foreach($_POST['sport_list'] as $selected) {

                $insert_custsports_qry = $conn->prepare("INSERT INTO customer_subscribed_sports(customer_id, sport_id) VALUES (:custid, :sport_id)"); 
                $QryArr = array(":custid"=>$custid, ":sport_id"=>$selected);
                $insert_custsports_qry->execute($QryArr);

            }
        }

        if($insertcust){
            header('Location:customerlist.php?msg=1');
            exit;
        }

    }

} else {
    
    if (isset($_POST['addsubmit'])) {
        
        if(!empty($_POST['sport_list'])) {

            $delete_custsoprt_qry = $conn->prepare("delete from customer_subscribed_sports where customer_id=:custid");
            $QryArr = array(":custid"=>$custid);
            $delete_custsoprt_qry->execute($QryArr);

            foreach($_POST['sport_list'] as $selected) {
                $insert_custsoprt_qry = $conn->prepare("INSERT INTO customer_subscribed_sports(customer_id, sport_id) VALUES (:custid, :selected)");
                $QryArr = array(":custid"=>$custid, ":selected"=>$selected);
                $insert_custsoprt_qry->execute($QryArr);
            }

        }

        $bg=$_POST['bgold'];
        $adv=$_POST['advold'];

        if (isset($_FILES["bg"]["type"])) {
            if ($_FILES["bg"]["name"]=='') {
                $bg=$_POST['bgold'];
            } else {
                $validextensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["bg"]["name"]);
                $file_extension = end($temporary);
                if ((($_FILES["bg"]["type"] == "image/png") || ($_FILES["bg"]["type"] == "image/jpg") || ($_FILES["bg"]["type"] == "image/jpeg")
                ) && ($_FILES["bg"]["size"] < 3000000)//Approx. 300kb files can be uploaded.
                && in_array($file_extension, $validextensions)) {
                    if ($_FILES["bg"]["error"] > 0) {
                        echo "Return Code: " . $_FILES["bg"]["error"] . "<br/><br/>";
                    } else {
                        $n=date("ymdihs");
                        $sourcePath = $_FILES['bg']['tmp_name']; // Storing source path of the file in a variable
                        $targetPath =preg_replace('/\s+/', '',"uploads/background/".$n.$_FILES['bg']['name']); // Target path where file is to be stored
                        move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                        $bg=preg_replace('/\s+/', '',$n.$_FILES['bg']['name']);
                    }
                }
            }
        }

        if (isset($_FILES["adv"]["type"])) {
            if ($_FILES["adv"]["name"]=='') {
                $adv=$_POST['advold'];
            } else {
                $validextensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["adv"]["name"]);
                $file_extension = end($temporary);
                if ((($_FILES["adv"]["type"] == "image/png") || ($_FILES["adv"]["type"] == "image/jpg") || ($_FILES["adv"]["type"] == "image/jpeg")
                ) && ($_FILES["adv"]["size"] < 3000000)//Approx. 300kb files can be uploaded.
                && in_array($file_extension, $validextensions)) {
                    if ($_FILES["adv"]["error"] > 0) {
                        echo "Return Code: " . $_FILES["adv"]["error"] . "<br/><br/>";
                    } else {
                        $n=date("ymdihs"); 
                        $sourcePath = $_FILES['adv']['tmp_name']; // Storing source path of the file in a variable
                        $targetPath =preg_replace('/\s+/', '',"uploads/ad/".$n.$_FILES['adv']['name']); // Target path where file is to be stored
                        move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                        $adv=preg_replace('/\s+/', '',$n.$_FILES['adv']['name']);
                    }
                }
            }
        }

        $ga = $_POST['ga'];           

        $update_custinfo_qry = $conn->prepare("update customer_info set name='$OrganizationName', abbrevation='$Nickname', address='$Address', city='$City', state='$State', zip='$Zip', customer_type='$OrgType', site_url='$weburl', scoring_platform='$Scoring', password='$password', additional_info='$addinfo', contact_name='$contact_name', contact_position='$contact_position', contact_address='$contact_address', contact_city='$contact_city', contact_state='$contact_state', contact_zip='$contact_zip', user_type='user', contact_phone='$contact_phone',contact_email='$email',email='$email', video='$video',bgimg='$bg',conference='$conference',adv='$adv',site_template='$sitetemp',ga='$ga',payment_schedule='$payschedule',livescoring_template='$livescoring',renewal_date='$renewdate' where id='$custid'");
        $update_custinfo_qry->execute();

        if ($video==0) {

            $custqry = $conn->prepare("select * from customer_video where customer_id=:custid");
            $QryArr = array(":custid"=>$custid);        
            $custqry->execute($QryArr);
            $row = $custqry->fetch(PDO::FETCH_ASSOC);
            if (count($row) > 0) {
                $update_custvideo_qry = $conn->prepare("update customer_video set streams=:streams, bandwidth=:bandwidth, storage=:storage, subscription=:subscription, rate=:rate where customer_id=:custid");
                $QryArr = array(":streams"=>$streams, ":bandwidth"=>$bandwidth, ":storage"=>$storage, ":subscription"=>"false", ":rate"=>$rate, ":custid"=>$custid);
                $update_custvideo_qry->execute($QryArr);
            }
        }
        if ($video==1) {

            $custqry = $conn->prepare("select * from customer_video where customer_id=:custid");
            $QryArr = array(":custid"=>$custid);        
            $custqry->execute($QryArr);
            $row = $custqry->fetch(PDO::FETCH_ASSOC);
            if (count($row) > 0) {

                $update_custvideo_qry = $conn->prepare("update customer_video set streams='$streams',bandwidth='$bandwidth',storage='$storage',subscription='true',rate='$rate' where customer_id='$custid'");
                $QryArr = array(":streams"=>$streams, ":bandwidth"=>$bandwidth, ":storage"=>$storage, ":subscription"=>"true", ":rate"=>$rate, ":customer_id"=>$custid);
                $update_custvideo_qry->execute($QryArr);
            } else {
                $insert_custinfo_qry = $conn->prepare("insert into customer_video set customer_id='$custid',streams='$streams',bandwidth='$bandwidth',storage='$storage'");
                $QryArr = array(":custid"=>$custid, ":streams"=>$streams, ":bandwidth"=>$bandwidth, ":storage"=>$storage);
                $insert_custinfo_qry->execute($QryArr);
            }

        }
        if($update_custinfo_qry){
            header('Location:customerlist.php?msg=4');
            exit;
        }
    }    
}

include_once('header.php');
?>
<link href="assets/custom/css/addcustomer.css" rel="stylesheet" type="text/css" />   

    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="portlet light " id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-red"></i>
                                <span class="caption-subject font-red bold uppercase"> Add customer -
                                    <span class="step-title"> Step 1 of 5 </span>
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal" action="#" id="addcustomerform" method="POST" enctype="multipart/form-data">
                            <div class="form-wizard">
                                <div class="form-body padding-top-remove">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <li>
                                            <a href="#tab1" data-toggle="tab" class="step">
                                                <span class="number newnumber"> 1 </span>
                                                <span class="desc steptabs">
                                                    <i class="fa fa-check"></i> Organization</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number newnumber"> 2 </span>
                                                <span class="desc steptabs">
                                                    <i class="fa fa-check"></i> Video</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step ">
                                                <span class="number newnumber"> 3 </span>
                                                <span class="desc steptabs">
                                                    <i class="fa fa-check"></i> Contact</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab4" data-toggle="tab" class="step ">
                                                <span class="number newnumber"> 4 </span>
                                                <span class="desc steptabs">
                                                    <i class="fa fa-check"></i> Billing</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab5" data-toggle="tab" class="step active">
                                                <span class="number newnumber"> 5 </span>
                                                <span class="desc steptabs">
                                                    <i class="fa fa-check"></i> Confirm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! 
                                        </div>
                                        <div class="tab-pane active" id="tab1"> 
                                            <div class="col-md-12 customer-details">
                                                <div class="col-md-4 organ-left">
                                                    <div class="form-group form-element">
                                                        <label for="OrganizationName" class="control-label">Organization name
                                                            <span class="required"> * </span>
                                                        </label>                                                
                                                        <input type="text" class="form-control input-sm" id="OrganizationName" name="OrganizationName" value="<?php echo $NameDB; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="Nickname">Nick name</label>
                                                        <input type="text" class="form-control input-sm" id="Nickname" name="Nickname" value="<?php echo $AbbrDB; ?>" />     
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="Address">Address</label>      
                                                        <input type="text" class="form-control input-sm" name="Address" id="Address" placeholder="" value="<?php echo $AddressDB; ?>">        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 customer-details">
                                                <div class="col-md-4 organ-left">
                                                    <div class="form-group form-element">
                                                        <label for="City">City                                                           
                                                        </label>                                                
                                                        <input type="text" class="form-control input-sm" name="City" id="City" placeholder="" value="<?php echo $CityDB; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 organ-left">
                                                    <div class="col-md-6 ">
                                                        <div class="form-group form-element">
                                                            <label for="State">State</label>
                                                            <input type="text" class="form-control input-sm" name="State" id="State" placeholder="" value="<?php echo $StateDB; ?>">    
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-element-zip">
                                                        <div class="form-group form-element ">
                                                            <label for="Zip">Zip</label>      
                                                            <input type="text" class="form-control input-sm" name="Zip" id="Zip" placeholder="" value="<?php echo $ZipDB; ?>">          
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="web_url" class="control-label">Website URL
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" onblur="weburlValidator(<?php echo $custid; ?>)" name="weburl" id="web_url" placeholder="website.com" aria-describedby="sizing-addon1" value="<?php echo $SitUrlDB;?>" >
                                                        <span id="uErrorSpan" ></span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 customer-details">
                                                <div class="col-md-4 organ-left">
                                                    <div class="form-group form-element">
                                                        <label for="sport" class="control-label sporttitle" >Sport (Check all that apply)
                                                            <span class="required"> * </span>
                                                        </label>                                                
                                                        <?php
                                                        if ($mode == "edit") { 
                                                            $qry = $conn->prepare("select * from customer_subscribed_sports where customer_id=:id");
                                                            $qryarr = array(":id"=>$id);        
                                                            $qry->execute($qryarr);
                                                            $fetchsport = $qry->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($fetchsport as $val) {
                                                                $sportcodes[] = $val['sport_id'];
                                                            }
                                                        } else {
                                                            $sportcodes = array();
                                                        }                                           
                                                        ?></br>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 leftrightpaddingremove">
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox1" name="sport_list[]" class="check_box" value="4444" <?php echo  in_array('4444',$sportcodes)?'checked':''; ?> data-title="Basketball"> Basketball
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox2" name="sport_list[]" class="check_box" value="4443" <?php echo  in_array('4443',$sportcodes)?'checked':''; ?> data-title="Football"> Football
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox3" name="sport_list[]" class="check_box" value="4441" <?php echo  in_array('4441',$sportcodes)?'checked':''; ?> data-title="Baseball"> Baseball
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 leftrightpaddingremove">
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox3" name="sport_list[]" class="check_box" value="4442" <?php echo  in_array('4442',$sportcodes)?'checked':''; ?> data-title="Softball"> Softball
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox3" name="sport_list[]" class="check_box" value="4445" <?php echo  in_array('4445',$sportcodes)?'checked':''; ?> data-title="Soccer"> Soccer
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 leftrightpaddingremove">
                                                                <label class="mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" id="inlineCheckbox3" name="sport_list[]" class="check_box" value="4446" <?php echo  in_array('4446',$sportcodes)?'checked':''; ?> data-title="4446"> Volleyball
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>                                                        
                                                        <span id="sportvalidate"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="OrgType" class="control-label">Organization Type
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <?php 
                                                        $custtype = $conn->prepare("select * from customer_organization_type");
                                                        $custtype->execute();
                                                        $custrow = $custtype->fetchAll(PDO::FETCH_ASSOC);
                                                        if (isset($CustTypeDB) && $CustTypeDB == "") {
                                                            $disabled = "disabled";
                                                            $customer_type = "4";
                                                        } else { 
                                                            $customer_type = $CustTypeDB;
                                                        }?>
                                                        <select class="form-control input-sm" name="OrgType" id="OrgType" <?php echo $disabled; ?>>
                                                            <option value="">---Select---</option>
                                                            <?php
                                                            foreach ($custrow as $res) {?>
                                                                <option value="<?php echo $res['id']; ?>" <?php echo ($res['id'] == $customer_type) ? "selected" : ""  ?> ><?php echo $res['name']; ?></option >
                                                            <?php }?>                                                    
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                   <div class="form-group form-element">
                                                        <label for="Scoring" class="control-label">Scoring Platform
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <select class="form-control input-sm" name="Scoring" id="Scoring">
                                                            <option value="">---Select---</option>
                                                            <option value="Turbostats" <?php echo ($ScoringPlatFormDB == "Turbostats") ? "selected" : ""  ?> >Turbostats</option>
                                                            <option value="Dakstats" <?php echo ($ScoringPlatFormDB == "Dakstats") ? "selected" : ""  ?> >Dakstats</option>
                                                        </select> 
                                                    </div>
                                                </div>                                        
                                            </div> 
                                            <div class="col-md-12 customer-details">
                                                <div class="col-md-4 organ-left conftop">
                                                    <div class="form-group form-element" >
                                                        <label for="conferencelist">Conference</label>
                                                        <select class="form-control input-sm" name="conference" id="conferencelist">
                                                            <option value="">---Select---</option>
                                                            <?php 
                                                            $custconf = $conn->prepare("select distinct(conference) from customer_info where conference!=''");
                                                            $custconf->execute();
                                                            $custrow = $custconf->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($custrow as $res) {?>
                                                                <option value="<?php echo $res['conference']; ?>" <?php echo ($res['conference'] == $ConferenceDB) ? "selected" : ""  ?> ><?php echo $res['conference']; ?></option>
                                                            <?php }?>
                                                            <option value="addnew">Add New</option>            
                                                        </select>
                                                        <!-- Conference Modal -->
                                                        <div class="modal fade " id="ConferenceModal" role="dialog">
                                                            <div class="modal-dialog">               
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h2 class="modal-title" id="">Add New Conference</h2>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="form-group form-element">    
                                                                            <label for="adddivtext">Enter New Conference</label>
                                                                            <input type="text" id="adddivtext" maxlength="10" class="form-control " name="adddivtext" />             
                                                                        </div>
                                                                        <input type="button" name="adddivbtn" class="btn btn-primary" value="Add" id="adddivbtn">      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                               
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="password" class="control-label">Password
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" name="password" id="password"placeholder="" value="<?php echo $PasswordDB;?>"> 
                                                    </div>                                                
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="confirm_password" class="control-label">Retype Password
                                                            <span class="required"> * </span>
                                                        </label>                                                
                                                        <input type="text" class="form-control input-sm" name="confirm_password" id="confirm_password" placeholder="" value="<?php echo $PasswordDB;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab2">                                       
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Video</label>
                                                <div class="col-md-4">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="video" value="1" checked data-title="Yes" <?php echo ($VideoDB == "1") ? "checked" : "" ?>> Yes
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="video" value="0" data-title="No" <?php echo ($VideoDB == "0") ? "checked" : "" ?>> No
                                                        <span></span>
                                                    </label>                                    
                                                </div>
                                            </div>
                                            <?php 
                                                $qry = $conn->prepare("select * from customer_video where customer_id=:customer_id");
                                                $qryarr = array(":customer_id"=>$idDB);
                                                $qry->execute($qryarr);
                                                $fetch = $qry->fetch(PDO::FETCH_ASSOC);
                                            ?>
                                            <div class="form-group">
                                                <label for="streams" class="control-label col-md-3">Streams</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm" name="streams" id="streams" placeholder="" value="<?php echo ($fetch['streams'])? $fetch['streams'] : 0 ?>"> 
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label for="storage" class="control-label col-md-3">Storage</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm" name="storage" id="storage" placeholder="" value="<?php echo ($fetch['storage']) ? $fetch['storage'] : 0 ?>"> 
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label for="bandwidth" class="control-label col-md-3">Bandwidth</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm" name="bandwidth" id="bandwidth" placeholder="" value="<?php echo ($fetch['bandwidth']) ? $fetch['bandwidth'] : 0 ?>"> 
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label for="rate" class="control-label col-md-3">Rate</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-sm" name="rate" id="rate" placeholder="" value="<?php echo ($fetch['rate']) ? $fetch['rate'] : 0 ?>"> 
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label for="addinfo" class="control-label col-md-3">Additional Information</label>
                                                <div class="col-md-4">
                                                    <textarea class="form-control" rows="3" name="addinfo" id="addinfo" ><?php echo $AddInfoDB;?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3">

                                            <div class="col-md-12 customer-details">                                            
                                                <div class="col-md-4 organ-left">
                                                    <div class="col-md-6 ">
                                                        <div class="form-group form-element">
                                                            <label for="contact_name" class="control-label">Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm" name="contact_name" placeholder="" id="contact_name" value="<?php echo $ConNameDB;?>">    
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-element-zip">
                                                        <div class="form-group form-element ">
                                                            <label for="contact_position">Position</label>    
                                                            <input type="text" class="form-control input-sm" name="contact_position" placeholder="" id="contact_position" value="<?php echo $ConPosDB;?>">          
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 organ-left">
                                                    <div class="form-group form-element">
                                                        <label for="contact_address">Address</label>
                                                        <input type="text" class="form-control input-sm" name="contact_address" placeholder="" id="contact_address" value="<?php echo $ConAddDB;?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="contact_city">City</label>
                                                        <input type="text" class="form-control input-sm" name="contact_city" placeholder="" id="contact_city" value="<?php echo $ConCityDB;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 customer-details">                                            
                                                <div class="col-md-4 organ-left">
                                                    <div class="col-md-6 ">
                                                        <div class="form-group form-element">
                                                            <label for="contact_state">State</label>
                                                            <input type="text" class="form-control input-sm" name="contact_state" placeholder="" id="contact_state"  value="<?php echo $ConStateDB;?>">   
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-element-zip">
                                                        <div class="form-group form-element ">
                                                            <label for="contact_zip">Zip</label>
                                                            <input type="text" class="form-control input-sm" name="contact_zip" placeholder="" id="contact_zip" value="<?php echo $ConZipDB;?>">          
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 organ-left">
                                                    <div class="form-group form-element">
                                                        <label for="mask_phone">Phone</label>
                                                        <input type="text" class="form-control input-sm" name="contact_phone" id="mask_phone" placeholder="" value="<?php echo $ConPhoneDB;?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-element">
                                                        <label for="email" class="control-label">Email
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" name="email" id="email" placeholder="" value="<?php echo $ConEmailDB;?>"> 
                                                    </div>
                                                </div>
                                            </div>
                                                                                    
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <div class="form-body">
                                                <div class="form-group" id="sitetempreqlab">
                                                    <label for="sitetempreq" class="control-label col-md-3">Site Template
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <select class="form-control input-sm" name="sitetemp" id="sitetempreq">
                                                            <option value="">---Select---</option>

                                                            <?php 
                                                            $qry = $conn->prepare("select * from customer_template");
                                                            $qry->execute();
                                                            $custtmp = $qry->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($custtmp as $res) {?>
                                                                <option value="<?php echo $res['site_template']; ?>" <?php echo ($res['site_template'] == $SitTempDB) ? "selected" : ""  ?> ><?php echo $res['site_template']; ?></option>
                                                            <?php }?>
                                                        </select> 
                                                    </div>                               
                                                </div>
                                                <div class="form-group" id="sitetemplab">
                                                    <label for="sitetemp" class="control-label col-md-3">Site Template</label>
                                                    <div class="col-md-4">                               
                                                        <select class="form-control input-sm" name="sitetempreq" id="sitetemp" disabled>
                                                            <option value="">---Select---</option>

                                                            <?php 
                                                            $qry = $conn->prepare("select * from customer_template");
                                                            $qry->execute();
                                                            $custtmp = $qry->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($custtmp as $res) {?>
                                                                <option value="<?php echo $res['site_template']; ?>" <?php echo ($res['site_template'] == $SitTempDB) ? "selected" : ""  ?> ><?php echo $res['site_template']; ?></option>
                                                            <?php }?>                                                        
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label for="livescoring" class="control-label col-md-3">Live Scoring Template</label>
                                                    <div class="col-md-4">
                                                        <select class="form-control input-sm" name="livescoring" id="livescoring">
                                                            <option value="">---Select---</option>

                                                            <?php 
                                                            $qry = $conn->prepare("select * from customer_template where live_scoring_template <>'' ");
                                                            $qry->execute();
                                                            $custtmp = $qry->fetchAll(PDO::FETCH_ASSOC);         
                                                            foreach ($custtmp as $res) {?>
                                                                <option value="<?php echo $res['live_scoring_template']; ?>" <?php echo ($res['live_scoring_template'] == $LiveScoringTemDB) ? "selected" : ""  ?> ><?php echo $res['live_scoring_template']; ?></option>
                                                            <?php }?>                                                       
                                                        </select>
                                                    </div>                                        
                                                </div>
                                                <div class="form-group reduce-bottom">
                                                    <label class="control-label col-md-3">Payment Schedule</label>
                                                    <div class="col-md-4">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="payschedule" value="Monthly" data-title="Monthly" <?php echo ($PaySchedDB == "Monthly") ? "checked" : "" ?> checked > Monthly
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="payschedule" value="Annually" data-title="Annually" <?php echo ($PaySchedDB == "Annually") ? "checked" : "" ?> > Annually
                                                            <span></span>
                                                        </label>                                    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="renewdate" class="control-label col-md-3">Renewal Date
                                                    </label>
                                                    <div class="col-md-4">                                                    
                                                        <input type="text" class="form-control" name="renewdate" id="renewdate" placeholder="mm/dd/yyy" value="<?php echo $RenewalDB;?>"> 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="button" class="btn mailgreenbtn" id="renewbutton">
                                                        <input type="hidden" value="" id="time-holder" name="newrenewdate">
                                                            <i class="fa fa-envelope-o"></i> Send Renewal Notice
                                                        </button> 
                                                    </div>
                                                    
                                                </div>
                                                <?php if ($mode == "edit") { ?>
                                                <div class="form-group">
                                                    <label for="bg" class="control-label col-md-3">Background</label>
                                                    <div class="col-md-4">
                                                        <input type="file" name="bg" id="bg"><span><?php echo $BgImgDB; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="adv" class="control-label col-md-3">Advertisement</label>
                                                    <div class="col-md-4">
                                                        <input type="file" name="adv" id="adv"><span><?php echo $AdvDB; ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ga" class="control-label col-md-3">Google Analytics Code(don't use &lt;script&gt; tags)</label>
                                                    <div class="col-md-4">
                                                        <textarea class="form-control" rows="3" name="ga" id="ga" ><?php echo $GaDB;?></textarea>
                                                    </div>
                                                </div>
                                                <input type="hidden" value="<?php echo $row['bgimg'] ?>" name="bgold"/>
                                                <input type="hidden" value="<?php echo $row['adv'] ?>" name="advold"/>
                                                <?php } ?>
                                            </div>                                        
                                        </div>
                                        <div class="tab-pane" id="tab5">
                                            <h3 class="block">Confirm your account</h3>
                                            <h4 class="form-section">Organization</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Username:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="OrganizationName"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sport:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="sport_list[]"> </p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Organization Type:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="OrgType"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Website URL:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="weburl"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Scoring Platform:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="Scoring"> </p>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Video</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Video:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="video"> </p>
                                                </div>
                                            </div>                                        
                                            <h4 class="form-section">Contact</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="contact_name"> </p>
                                                </div>
                                            </div> 
                                            <h4 class="form-section">Billing</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Site Template:</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static" data-display="sitetemp"> </p>
                                                </div>
                                            </div>                                        
                                        </div>
                                        <div class="form-actions border-re">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <a href="javascript:;" class="btn button-previous customredbtn">
                                                        <i class="fa fa-angle-left"></i> Back </a>
                                                    <a href="javascript:;" class="btn btn-outline button-next customgreenbtn"> Continue
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                    <a href="javascript:;" ><input type="submit" name="addsubmit" class="btn button-submit customgreenbtn" value="Submit"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>

<script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/form-wizard.js" type="text/javascript"></script>
<script src="assets/custom/js/addcustomer.js" ></script>    
