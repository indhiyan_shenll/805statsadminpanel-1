$(document).ready(function(){

    $('#sample_5').DataTable({
         "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
        "aaSorting": [[0,'desc']], 
        "language": {
            "zeroRecords": "No Customer(s) found.",
            "infoEmpty": "No Customer(s) found."
        },            
        // "aoColumnDefs": [ { "bSortable": false, "aTargets": [6,7,8] } ], 
    });

    $( "#resetbtn" ).click(function() {        
        document.location='customerlist.php';       
    });

    // $('.dt-buttons').hide();

    $(document).on("click", "#openBtn", function () {       

        var customerid = $(this).attr('customerid');
        var weburl = $(this).attr('weburl');
        $('#custid').val(customerid);
        $('#weburl').val(weburl);

    });

    $( "#organizationsearch" ).keyup(function() {

        $(".loadingsection").show();
        $('.customerlistparent').hide();

        var OrganName = $("#organizationsearch").val();
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        $("#hdnsearch").val(OrganName);
        var RecordsPerPage = $("#RecordsPerPage").val();
         $.ajax({
            url:"filter_customers.php",  
            method:'POST',
            data:{searchbyorganization: OrganName, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
            success:function(data) {
                $('.customerlistparent').html(''); 
                $('.customerlistparent').html(data);

                $(".loadingsection").hide();
                $('.customerlistparent').show();
            }
          }); 
    });

$('#conferencelist').change(function(){
if ((this.value) == 'addnew') {
  
  $('#ConferenceModal').modal('show');        
    }
});

$("#adddivbtn").click(function(){
   var newvalue=  $("#adddivtext").val();
       if(newvalue!=""){  
 $('#conferencelist').append($('<option/>', { 
        value: newvalue,
        text : newvalue,
selected:'selected' 
    }));
 

            $("#conferencelist option[value='']").removeAttr("selected","selected");
       }
       else{
            alert('Field can not be left blank');
            return false;
            $("#conferencelist option[value='']").attr("selected","selected");
       }       
  $('#ConferenceModal').modal('hide');
 });
$(".closemodal").click(function(){
      $("#divisionlist option[value='']").attr("selected","selected");
      $('#ConferenceModal').modal('hide'); 
$('#rulelist').prop("disabled", true);
 });

// $(".redbtn").click(function(){
//   alert();
//   $('.deleteitems').show(); 
//  });

// $("#deletecust").click(function(){
//  $('.deleteitems').hide();
// });

// $(".closemodal").click(function(){
//      $('.deleteitems').hide();
//  });

 $("#cancelbtn").click(function(){
   // window.location = "customerlist.php" ;
   window.location = "index.php" ;
 });
$('.check_box').change(function(){
    
    if($('.check_box:checked').length>1) {
        
        $('#OrgType option[value=4]').attr('selected','selected');
        $('#OrgType').prop("disabled", true);
        $('#orgtype .form-error' ).hide(); 
        $('#sitetemplab').hide();
        $('#sitetempreqlab').show();
        $('#sitetemp').hide();
        $('#sitetempreq').show();
    } else {         
        $('#OrgType').prop("disabled", false);
        $('#OrgType option[value="4"]').removeAttr('selected','selected');
        $('#sitetemplab').show();
        $('#sitetempreqlab').hide();
        $('#sitetemp').show();
        $('#sitetempreq').hide();
        $('#sitetempreq .form-error' ).hide(); 
    }
    
});


  $('#sitetemplab').show();
  $('#sitetempreqlab').hide();
  $('#sitetemp').show();
  $('#sitetempreq').hide();

$('#OrgType').change(function(){
  if($(this).val() == '5'){ 

    $('#sitetemplab').show();
  $('#sitetempreqlab').hide();
  $('#sitetemp').show();
  $('#sitetempreq').hide();
 
$( '#sttemp .form-error' ).hide();

  }else{
  $('#sitetemplab').hide();
  $('#sitetempreqlab').show();
  $('#sitetemp').hide();
  $('#sitetempreq').show();

}
});


 $("#weburl").change(
           function()
           {
               var textbox = $(this);
               if (textbox.val().indexOf("http://") == 0)
                   textbox.val(textbox.val().substring(7));
               if (textbox.val().indexOf("https://") == 0)
                   textbox.val(textbox.val().substring(8));
           });

});


// $("#customerform").validate();
// if ($('#customerform').valid())
//     $('#customerform').submit();
 


$("#renewbutton").click(function(){
   senem()
});


function senem() {
var email = document.getElementById("email").value;
    $.ajax({
        type: "POST",
        url: "sendemail.php",
        data: "mailid=" + email,
        success: function(){
            alert("Email Sent Successfully");
        }
    });
}

$(function(){
        $('#renewbutton').click(function(){
          var time = new Date();                
          $('#time-holder').val(time.toDateString());  
        });
    }


);



// $(function(){
//     $("#web_url").click(function(){
//        weburlValidator()
//     });
// });

function weburlValidator(cid){    
       var wurl= document.getElementById('web_url').value;
       // alert(wurl);
       
    if(wurl!= "")
    {

        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5

          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {

                var value = xmlhttp.responseText;
                // console.log(value);
                if(value== 1)
                {
                    document.getElementById('uErrorSpan').innerHTML="This Website URL already exist";
                    email.focus();
                    return false;
                }
                else
                {
                    document.getElementById('uErrorSpan').innerHTML="";
                }
            }
          }
     
        // xmlhttp.open("GET","http://805stats.com/panel/checkurl.php?p="+wurl,true);
        // xmlhttp.open("GET","checkurl.php?p="+wurl,true);
        xmlhttp.open("GET","checkurl.php?p="+wurl+"&cid="+cid,true);
        xmlhttp.send();
    }

}
