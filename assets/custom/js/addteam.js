$(document).ready(function(){

	$('#resendmail').hide();
    $( "#teamlogin-yes" ).click(function() {

        $('#allow-team-login-edit').show();
        $('#cancelbtn').css('margin-left', '0px');
        $('#resendmail').show();
        $('#login_email').attr('data-validation', 'required');
        $('#login_password').attr('data-validation', 'required');
        
    });

    $( "#teamlogin-no" ).click(function() {
        
        $('#allow-team-login-edit').hide();
        $('#cancelbtn').css('margin-left', '15px');
        $('#resendmail').hide();
        $('#login_email').attr('data-validation',false);
        $('#login_password').attr('data-validation',false);
    });

    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }

    $( ".generate" ).click(function() {
        var gen_password = randomPassword(8);
        $('#login_password').val(gen_password);
    });

    $("#cancelbtn").click(function(){
        var sportnam = $("#sportn").val();
        window.location = "team_list.php?sport="+sportnam ;
    });
	
	$('#rulelist').prop("disabled", true);

	$("#cancelbtn").click(function(){
		var sportnam = $("#sportn").val();
		// window.location = "teamlist.php?sport="+sportnam ;
		window.location = "team_list.php?sport="+sportnam;
	});

	//Modal Popup for the adding Division
	$('#divisionlist').change(function(){
		if ((this.value) == 'addnew') {
			// $('.adddivbox').show(); 
			$('#DivisionModal').modal('show');        
		}
	});	

	$("#adddivbtn").click(function(){

		var newvalue=  $("#adddivtext").val();
		
		if(newvalue!=""){  
			$('#divisionlist').append($('<option/>', { 
				value: newvalue,
				text : newvalue,
				selected:'selected' 
			}));
			$("#divisionlist option[value='']").removeAttr("selected","selected");
		}else{
			 alert('Field can not be left blank');
			 return false;
			 $("#divisionlist option[value='']").attr("selected","selected");
		}       
		// $('.adddivbox').hide();
		$('#DivisionModal').modal('hide');
		$('#rulelist').prop('disabled', false);    
	});

	$(".closemodal").click(function(){

		$("#divisionlist option[value='']").attr("selected","selected");
		// $('.adddivbox').hide();
		$('#DivisionModal').modal('hide');
		$('#rulelist').prop("disabled", true);
	});

	//Modal Popup for the adding Conference 
	$('#conferencelist').change(function(){
		if ((this.value) == 'addnew') {
			$('#ConferenceModal').modal('show');
			// $('.addconfbox').show();         
		}
	});	

	$("#addconfbtn").click(function(){

		var newvalue=  $("#addconftext").val();
		
		if(newvalue!=""){  
			$('#conferencelist').append($('<option/>', { 
				value: newvalue,
				text : newvalue,
				selected:'selected' 
			}));
			$("#conferencelist option[value='']").removeAttr("selected","selected");
		}else{
			 alert('Field can not be left blank');
			 return false;
			 $("#conferencelist option[value='']").attr("selected","selected");
		}       
		$('#ConferenceModal').modal('hide');
		// $('.addconfbox').hide();
		//$('#rulelist').prop('disabled', false);    
	});

	$(".closeconfmodal").click(function(){
		
		$("#conferencelist option[value='']").attr("selected","selected");
		$('#ConferenceModal').modal('hide');
		// $('.addconfbox').hide(); 
		//$('#rulelist').prop("disabled", true);

	});


});




$(document).ready(function(){	

	jQuery.validator.addMethod("lettersonly", function(value, element) {	
		return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
	}, "Do not add special characters or number"); 

	jQuery.validator.addMethod("uploadFile", function (val, element) {

		if (element.files[0]) {
			var size = element.files[0].size;	
			if (size > 300000) {
				return false
			} else {
				return true
			}
		} else {
			return true
		}

		}, "file size is large than 300kb");
	

});

$(document).ready(function(){

    $("#teamform").validate({       
         rules: {
              teamname:{
                 required:true,
                 lettersonly:true,
              },
             printname:{
                 required:true,
                 lettersonly:true,
              }, 
             abbr:{
                 required:true,
                
              },
             conferencelist:{
                 required:true,
             },
             divisionlist:{
                 required:true,
             },
             rulelist:{
                 required:true,
             },
             login_email:{
                required:true,
                email:true,
                unique: true,
            },
            

            login_password:{
                required:true,
             },
             file:{
                required:true,
                extension: true,
                uploadFile:true,
              },
             
             
              
         }, 
         messages: {
          teamname:{
            required: "Please enter team name",            
          },
           printname:{
            required: "Please enter print name",           
          },
           abbr:{
            required: "Please enter abbreviation",   
           
          },       
          conferencelist:{
            required: "Please select conference",    
          },
          divisionlist:{
            required: "Please select division",  
          },
          rulelist:{
            required: "Please select rule",  
          },
          login_email:{
             required: "Please enter email",
             email: "Please enter valid email",
             unique: "Email already exists"
         },
         login_password:{
             required: "Please enter password",
         },
          file:{
                required: "Select Image",
               	extension: "Invalid file type",
               	uploadFile: "file size is large than 300kb",    
          },
        
          
        }

     });    

});
    var response; 
    $.validator.addMethod('unique',function(value){

        var loginEmail = $("#login_email").val();
        var postCheck = "teamlogincheck";
        var teamID = $("#teamid").val();
        // alert(teamID);
        $.ajax({
            type: "GET",
            url: "filter_social_media.php",
            async: false,
            data: {"email": loginEmail, "teamid":teamID, "postcheck": postCheck },
      
        })
        .success(function(msg) {
                response = msg;
        });
        if(response == "notexists")
            return true;
        else
            return false;            
    });        

/*image uploading and priview */		

	function showimagepreview(input) {

		var fp = $("#file");
	    var lg = fp[0].files.length; // get length
	    var items = fp[0].files;
	    var fragment = "";
	    
	    if (lg > 0) {
	        for (var i = 0; i < lg; i++) {
	            var fileName = items[i].name; // get file name
	            var fileSize = items[i].size; // get file size 
	            var fileType = items[i].type; // get file type

	            if (fileType == "image/png" || fileType == "image/jpeg" || fileType == "image/jpg") {
	            	jQuery.validator.addMethod("extension", function(value, element) {
	            		return true;
					}, '');
	            } else {
	            	jQuery.validator.addMethod("extension", function(value, element) {
	            		return false;
					}, '');
	            }
	   
	        }
	      }

		if (input.files && input.files[0]) {

			var filerdr = new FileReader();

		filerdr.onload = function(e) {

			$('.imgprvw').attr('src', e.target.result);

		}

		filerdr.readAsDataURL(input.files[0]);

		}

	}

	function removeimage()
    {


        document.getElementById("file").value="";
        document.getElementById("pwv").setAttribute("src","images/add.png");
        // document.getElementById("oldfilename").value="";
    }

//    function deleteTeam(id,sportname){
// 	var answer = confirm('Are you sure you want to delete?');
// 	if(answer){ 
// 		window.location = "deleteteam.php?action=delete&did="+id+"&sportname="+sportname;
// 	}
// 	else{
// 		alert("Cancelled the delete!")
// 	}
// }