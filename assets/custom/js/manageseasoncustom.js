//Function used to reload conference
$(document).on('click','.managedivpopbtn', function(evt) {
	var $this = $(this);
	var FormArr  = $this.closest("#managedivformid").serialize();
	var SeasionId = $("#seasioniddivid").val();
	$this.closest("#managedivformid").hide();
	$this.closest(".modal-body").find("#loadingdivison").show();
	$( "#seasonmaincont_"+SeasionId).load( "update_confrence.php?"+FormArr, function( response, status, xhr ) {
		$("#loadingdivison").hide();
		$( "#managedivinmsg" ).show();
		
	});
});


$(document).ready(function () {

$.validator.addMethod('uniquediv',function(value)
 {
  var divisionname=value;  
  var post_type="divisionnamecheck";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"divisionname":divisionname},
		 dataType:"text",
	 });    
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);

$.validator.addMethod('uniqueconf',function(value)
 {
  var conferencename=value;  
  var post_type="conferencenamecheck";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"conferencename":conferencename},
		 dataType:"text",
	 }); 
		
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);

$.validator.addMethod('requiredcs',function(value){
	if(value==''){
		return false;
	}else{
		return true;
	}
},""
);
var res = '';
$.validator.addMethod('uniquedivision',function(value){
		var post_type="division_name_check";
		var divisionname=$("#divisionname").val();
		var divisionrulelist=$('#divisionrule').val();
		 var response; 
        $.ajax({
            method: "POST",
            url: "manageseasondivision-ajax.php",
			async:false,
            data:{"post_type":post_type,"division_name":divisionname,"division_rulelist":divisionrulelist},      
            }).success(function(response) {
				res = response;
                
            });
            if(res == "not-exists"){	
			  return true;
			}else{
			  return false;
			} 
    });

$("#divisionfrm").validate({
	 rules: {
		division:{requiredcs:true},
		divisionrule:{requiredcs:true,uniquedivision: true},	
		 },
		messages: {
		 division:{requiredcs:"Please enter division name"},
		 divisionrule:{requiredcs:"Please select division rule",uniquedivision:"Division name already exists"},			 
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			var SeasionId = $("#seasoniddiv").val();
			$form.hide();
			$("#DivisionModal").find("#loadingadddivisn").show();
			$( "#seasonmaincont_"+SeasionId).load( "add_newdivision.php?"+FormArr, function( response, status, xhr ) {	
				if(status=="success"){
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details saved successfully.");
				}else{
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details not saved.");
				}
				//$( "#DivisionModal" ).modal("hide");
				
			});
	}
 });	
	
	 
		

// changes file



/*$("#divisionfrm").validate({
	 rules: {
		 division:{requiredcs:true,uniquediv: true},		 
		 },
		messages: {
		 division:{requiredcs:"Please enter division name",uniquediv:"Division name already exists"},	 
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			var SeasionId = $("#seasoniddiv").val();
			$form.hide();
			$("#DivisionModal").find("#loadingadddivisn").show();
			$( "#seasonmaincont_"+SeasionId).load( "add_newdivision.php?"+FormArr, function( response, status, xhr ) {	
				if(status=="success"){
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details saved successfully.");
				}else{
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details not saved.");
				}
				//$( "#DivisionModal" ).modal("hide");
				
			});

		 }
	});*/
	


$('.addnewdivibtn').on('click', function(evt) {
	if (!$("#divisionfrm").validate()) { 
		return false;
	} 	
	$("#divisionfrm").submit();		
});

$('.addnewconferencebtn').on('click', function(evt) {
	if (!$("#conferencefrm").validate()) { 
		return false;
	} 	
	$("#conferencefrm").submit();		
});

$("#conferencefrm").validate({
	 rules: {
		 conference:{requiredcs:true,uniqueconf: true},		 
		 },
		messages: {
		 conference:{requiredcs:"Please enter conference name",uniqueconf:"Conference name already exists"},	 
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			
			var SeasionId = $("#seasonid").val();
			$form.hide();
			$("#ConferenceModal").find("#loadingaddconfer").show();
			$( "#seasonmaincont_"+SeasionId).load( "add_newconference.php?"+FormArr, function( response, status, xhr ) {			if(status=="success"){
					$("#loadingaddconfer").hide();
					$("#conferencestsmsg").show();
					$("#conferencestsmsg tr td").empty().append("Conference details saved successfully.");
				}else{
					$("#loadingaddconfer").hide();
					$("#conferencestsmsg").show();
					$("#conferencestsmsg tr td").empty().append("Conference details not saved.");
				}
				
				
			});

		 }
	});

});
//Function used to reload season 
$(document).on('click','.manageconfpopbtn', function(evt) {
	var $this = $(this);
	var FormArr  = $this.closest("#manageconfformid").serialize();
	
	var SeasionId = $("#seasionidconfid").val();
	$this.closest("#manageconfformid").hide();
	$this.closest(".modal-body").find("#loadingconference").show();
	$( "#seasonmaincont_"+SeasionId).load( "update_season.php?"+FormArr, function( response, status, xhr ) {		
		$("#loadingconference").hide();
		$( "#manageconfmsg" ).show();
		
	});
});

   $(document).on('click','.managedivisionbtn', function(evt) {
	  
	   $("#managedivModal").find("#managedivformid").show();
	   $("#loadingdivison,#managedivinmsg").hide();

	   var seasonid = $(this).attr("data-seasonid");
	   var ConferenceId = $(this).attr("data-conferenceid");

	   $("#seasioniddivid").val(seasonid);
	   $("#seasionidconfdivid").val(ConferenceId);
	   var HtmlWrap = $("#divisionwrap_"+ConferenceId).html();
		$("#managedivformcont").empty().append(HtmlWrap);
	});
  
	$(document).on('click','.manageconferencemodel', function(evt) {
		$("#loadingconference,#manageconfmsg").hide();
		$("#manageconfformid").show();
	   var seasonid = $(this).attr("data-seasonid");
	   $("#seasionidconfid").val(seasonid);
	   var HtmlWrap = $("#conferewrap_"+seasonid).html();
		$("#manageconfformcont").empty().append(HtmlWrap);
	});

	$(document).on("click", ".addconferencebtn", function(event){
		var SeasonId = $(this).attr("data-seasonid");		
		$('#seasonid').val(SeasonId);
		$("#loadingaddconfer,#conferencestsmsg").hide();
		$("#conferencefrm").show();
		$("#conferencename").val('');
		$('input[name=activeconference]').attr('checked', false);
	});
	$(document).on("click", ".adddivisionbtn", function(event){
		$("#loadingadddivisn,#divisionstsmsg").hide();		
		$("#divisionfrm").show();
		$("#divisionname").val('');
		$('.inactivedivision').attr('checked', 'checked');
		$('.seldivinrule').val('');
		

		var ConferenceId = $(this).attr("data-conferenceid");
		var SeasonId = $(this).attr("data-seasonid");		
		$('#conferenceid').val(ConferenceId);
		$('#seasoniddiv').val(SeasonId);
	});

	
	$(document).on("click", ".deletebtnconf", function(event){
		var $this = $(this);

		if(confirm("Are you sure want to delete this conference?")){
			var ConferenceId = $(this).attr("data-conferenceid");
			var SeasonId	 = $(this).attr("data-seasonid");
			
			$.ajax({
			  url: "deleteconference.php",
			  type: "POST",
			  data: {seasonid:SeasonId,conferenceid : ConferenceId},
			  success: function(data){
				$this.closest(".innertable").next(".innerdivtable").remove();
				$this.closest(".conferencebtns").remove();
			  }
			});
		}else{
			return false;
		}
	});
	$(document).on("click", ".deletebtndiv", function(event){
		var $this = $(this);
		var DivisionId = $(this).attr("data-divisionid");
		var ConferenceId = $(this).attr("data-conferenceid");
		var SeasionId = $(this).attr("data-seasionid");


		if(confirm("Are you aure want to delete this division?")){			
			$.ajax({
			  url: "deletedivision.php",
			  type: "POST",
			  data: {conferenceid:ConferenceId,divisionid : DivisionId,seasionid:SeasionId},
			  success: function(data){
				if(data=="success"){
					$this.closest("td").remove();
					$('.tooltip ').hide();
					$("#divisionwrap_"+ConferenceId).find('input:checkbox[value="' + DivisionId + '"]').attr('checked', false);
					
				}
			  }
			});
		}else{
			return false;
		}
	});

	$(document).on("click", ".deleteseasonbtn", function(event){
		var $this = $(this);
		if(confirm("Are you sure want to delete this season?")){
			var SeasonId = $(this).attr("data-seasonid");
			$.ajax({
			  url: "deleteseason.php",
			  type: "POST",
			  data: {seasonid:SeasonId},
			  success: function(data){
				$this.closest(".seasonwrapcont").remove();
			  }
			});
		}else{
			return false;
		}
	});


$(document).on('change','.divisionlistchk', function(evt) {
	var $this = $(this);
	if($this.is(":checked")) {
		var DivisionId   =  $this.val();
		var ConferenceId =  $("#seasionidconfdivid").val();
		var SeasonId     =  $("#seasioniddivid").val();

		var post_type    =  "divisionassignchk";
		var result=$.ajax({
			 type:"POST",
			 async:false,
			 url:"nameexistscheck.php",
			 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
			 dataType:"text",
		 }); 	
		console.log(result);
		if(result.responseText=="exists"){
			alert("This division already assigned to another conference");
			$this.attr("checked",false);
			return false;
			
		}else{
			return true;
		}	  
	}
});

$(document).on('change','.seasonnamesnew', function(evt) {
	var $this = $(this);
	if($this.is(":checked")) {		
		$(".popupmodelseason").hide();
		$this.closest(".mt-radio-list").next(".popupmodelseason").slideDown();
	}
});


$.validator.addMethod('uniqueseason',function(value)
 {
  var seasonname=value;  
  var post_type="seasonnamechk";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"seasonname":seasonname},
		 dataType:"text",
	 }); 
	
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);




$("#addseasonfrm").validate({
	 rules: {
		 seasonnamenew:{requiredcs:true,uniqueseason: true},		 
		 },
		messages: {
		 seasonnamenew:{requiredcs:"Please enter season name",uniqueseason:"Season name already exists"},	 
	   },
		submitHandler: function (form) {	
			 
			var SeasonId      = SeasonQry = FormArr = '';
			var seasonnamenew = $("#seasonnamenew").val();
			var Existsseason    = '';
			
			SeasonQry  = "&seasonnamenew="+encodeURIComponent(seasonnamenew);
			$('.seasonnamesnew').each(function () {
				if($(this).is(":checked")){		
					Existsseason  = $(this).val();
					FormArr = $(this).closest(".mt-radio-list").next(".popupmodelseason").find(".selectedseasontree").serialize();	
					SeasonQry  += "&existsseason="+Existsseason;
				}
			});			
			
			$("#addseasonfrm,#addseasonformcont").hide();
			$("#seasonModal").find("#loadingseason").show();
			$( '.seasonmainwrapper').load( "add_newseason.php?"+FormArr+SeasonQry, function( response, status, xhr ) {	
				console.log(xhr);
				if(xhr.statusText=="OK"){
					$( '.seasonmainwrapper').html(xhr.responseText);
					$("#addseasonformcont,#addseasonfrm").hide();
					$("#seasonstsnmsg").show();
					$("#seasonModal").find("#loadingseason").hide();
					$("#seasonstsnmsg tr td").empty().append("Season details saved successfully.");				
					return false;
				}
			});

		 }
	});


$(document).on('click','.addnewseasonbtn', function(evt) {
	if (!$("#addseasonfrm").validate()) { 
		return false;
	} 	
	$("#addseasonfrm").submit();		
});

$(document).on('click','.addseasonbtntop', function(evt) {
	$("#loadingseason,#seasonstsnmsg,.popupmodelseason,#seasonnamenew-error").hide();
	$("#addseasonformcont,#addseasonfrm").show();
	$("#seasonnamenew").val("");
	var HtmlCont  = $("#addseasonformcont").html();
	$("#addseasonformcont").empty().append(HtmlCont);

});

