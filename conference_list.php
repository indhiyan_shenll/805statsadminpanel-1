<?php 
include_once('connect.php');
include_once('session_check.php');
include_once('common_functions.php');

// session empty
if($_SESSION['loginid']=='')  {
    header('Location:login.php');
    exit;
}

$cid="";
if($_SESSION['loginid']!='')  {
  if($_SESSION['usertype']=='user') {
     $cid=$_SESSION['loginid'];
  }
  else{ 
    header('Location:login.php');
    exit;
  }
}

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$urlArr= explode('=',$actual_link); 


// Error msg Start Here
if(isset($_GET["msg"])){
  $msg            =   $_GET["msg"];
} else {
    $msg          =  "";
}
$alertclass = '';
if($msg==1){
    $message    =   "Conference has been added successfully.";
    $alertclass = "alert-success";
}
elseif($msg==2){
    $message    =   "Conference has been updated successfully.";
    $alertclass = "alert-success";
}
elseif($msg==3){
    $message    =   "Conference has been deleted successfully.";
    $alertclass = "alert-danger";
}

if(isset($_GET["cid"])){
    $c_id=base64_decode($_GET["cid"]);
    $sport=$_GET["sport"];
    $delResQry      = $conn->prepare("delete from customer_conference where id=:id");
    if($delResQry->execute(array(":id"=>$c_id))){
         header('Location:conference_list.php?msg=3&sport='.$sport);
         exit;
        
    }
}

// Delete Player End Here

if(isset($_POST["addupadte"])){
    $confere_name         =   $_POST['confere_name'];
    $sportna              =   $_POST['sport_namels']; 
    $sportls              =   $_POST['sport_name']; 
    $conference_id       = base64_decode($_POST["hnd_conferenceid"]);

    if($sportna!=""){
        $sportname=$sportna;
    }
    if($sportls !=""){
        $sportname=$sportls;
    }
    if($conference_id!=""){
        $update_results=array(":cid"=>$cid,":confere_name"=>$confere_name,":id"=>$conference_id);
        $updateQry="update customer_conference set customer_id=:cid,conference_name=:confere_name where id=:id";
        $prepupdateQry=$conn->prepare($updateQry);
        $updateRes=$prepupdateQry->execute($update_results);
        if($updateRes){
            header('Location:conference_list.php?msg=2&sport='.$sportname);
           exit;
        }
   }
    else{
        $insert_results=array(":cid"=>$cid, ":confere_name"=>$confere_name);
        $insertqry="insert into customer_conference(customer_id, conference_name) values(:cid, :confere_name)";
        $prepinsertqry=$conn->prepare($insertqry);
        $insertRes=$prepinsertqry->execute($insert_results);
        if($insertRes){
            header('Location:conference_list.php?msg=1&sport='.$sportname);
            exit;
        }
     }
}

//Get customer_subscribed_sports data Start here

$sports[]=array();
$sportslists = "select * from customer_subscribed_sports where customer_id=:cid";
$sportslistsqry = $conn->prepare($sportslists);
$sportslistsqry->execute(array(":cid"=>$cid));
$soprts_Count = $sportslistsqry->rowCount();
if($soprts_Count>0){
    $getResSports     =   $sportslistsqry->fetch();
    foreach($getResSports as $sportlist)
    {
        $sports[]= $getResSports['sport_id']; 
    }
}

//Get customer_subscribed_sports data End here

//Get Sport details start here
if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
      $getSportsRow=$get_sport_qry->fetch();
      $sportid= $getSportsRow['sportcode'];
    }
}
else{
    $sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
}
//print_r($_POST);

if(isset($_REQUEST["hdnsearch"])){
    $HiddenSearch = $_REQUEST["hdnsearch"];
    $HiddenSearchText = ( $HiddenSearch )? $HiddenSearch : $_POST['confere_name'] ;
    $HiddenSearchCondn = !empty($HiddenSearchText)? "and conference_name like '%$HiddenSearchText%'": "";
} else {
    $HiddenSearchCondn="";
}

//$HiddenSearchCondn = !empty($HiddenSearchText)? "and conference_name like '%$HiddenSearchText%'" : "" ;

/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php'); ?>
<link href="assets/custom/css/conferencelist.css" rel="stylesheet" type="text/css" />
<style type="text/css">

table.dataTable.no-footer {
    border-bottom: 0px solid #111; 
}
table.dataTable{
    border-collapse: collapse;
}
.error{
    color: red;
}
</style>
<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <form  method="post" id="searchplayedform">
                         
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo $cid ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                        <div class="portlet light col-md-5" style="padding: 15px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: right;margin-bottom:10px;">
                            <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                <div class="col-md-12" style="padding:0px">
                                    <div class="col-md-10 col-sm-10 col-xs-12 searchboxstyle" style="padding-right:0px">
                                        <div class="form-group">
                                            <input class="form-control border-radius" type="text" name="conferencename"  placeholder="Conference Name" id="conferencename" value="<?php echo $HiddenSearch; ?>"> 
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12 resetbtn style" style="text-align: right;padding: 0px ;">
                                        <div class="form-group">
                                            <!-- <input type="submit" id="searchbtn" name="searchbtnpost" class="btn btn-secondary " value="Search" name="search" style="font-size:12px;line-height:1.9;"> -->
                                            <a class="btn btn-danger resetbtn">Reset</a> 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->
            


            <?php
                if(!empty($message)){
                ?>
                <div class="alert <?php echo $alertclass; ?>" style="margin-bottom:10px;">
                <a class="close" data-dismiss="alert" href="#">x</a>
                <?php echo $message;?>
                </div>
                <?php
                }
                ?>
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF CONFERENCES                       
                            </h3>
                            <div class="pull-right">                                
                               <?php if(isset($_GET['sport'])){ ?>

                            <button  type="button" class="player_btn popup" style="margin-right: 14px;border-radius: 4px !important;">Add Conference</button> 
                            <input type="hidden" name="sportname" id="sportname" value="<?php echo $sportname; ?>">
                            <?php } else{ ?> <?php
                            if($soprts_Count==1)
                            {
                            if($sports[1]=='4444') { $ls='basketball'; } 
                            if($sports[1]=='4442') { $ls='softball'; } 
                            if($sports[1]=='4441') { $ls='baseball'; } 
                            if($sports[1]=='4443') { $ls='football'; } 
                            ?>

                           <!--  <button type="button" class="player_btn " style="margin-right: 14px;border-radius: 4px !important;" onclick="document.location='manage_conference.php?sport=<?php echo $ls; ?>'">Add Conference</button> --> 
                            <button  type="button" class="player_btn popup" style="margin-right: 14px;border-radius: 4px !important;">Add Conference</button>
                            <!-- <input type="hidden" name="sportname" id="sportnamels" value="<?php echo $ls; ?>"> -->
                            <?php } ?>

                            <?php } ?>
                                
                            </div>
                        </div>
                        <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Conference Information <span style="margin-right: 10%;" class="mode-color" id="mode"></span></h4>
                                    </div>
                                    <div class="modal-body" style="padding-bottom:7%;">
                                        
                                        <form role="form" name="frm_conference" id="frm_conference" method="POST">
                                            <input type="hidden" name="hnd_img" id="hnd_img" value="<?php echo  $image_db ?>">
                                            <input type="hidden" name="sport_name" id="sport_name" value="<?php echo $sportname; ?>">
                                            <input type="hidden" name="sport_namels" id="sport_namels" value="<?php echo $ls; ?>">
                                            <input type="hidden" name="hnd_conferenceid" id="hnd_conferenceid" >
                                            <input type="hidden" name="hnd_cid" id="hnd_cid">
                                            
                                            <input type="hidden" name="hndcoustid" id="hndcoustid" value="<?php echo $cid?>">
                                            <div class="portlet light  team_bio_portlet">
                                                <div class="portlet-body form"> 
                                                    <div class="form-body">
                                                        <div class="col-md-10 col-sm-10 col-xs-12 paddingremove confmodalpopup">
                                                            <div class="form-group  playerinfo_paddingleft">
                                                                <label>Conference Name: <span class="error">*</span></label>
                                                                <input class="form-control border-radius" type="text" name="confere_name" id="confere_name" placeholder="Conference Name" value="<?php echo $conference_name_db;?>" /> 
                                                            </div>
                                                            <div class="" style="text-align: left;">
                                                                <button type="submit" name="addupadte" class="btn btn-success conferencebtn" style="padding-top:5px;font-size:14px;" id="mode_btn">Save/Update</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal" style="font-size:14px;margin-left: 15px;">Cancel</button>

                                                            <!-- <div class="" style="text-align: center;">
                                                                <button type="submit" name="addupadte" class="btn btn-success conferencebtn" style="padding: 8px 10px !important;">Save/Update</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal" style="padding: 8px 10px;">Cancel</button> -->                                                            
                                                            </div>
                                                        </div> 
                                                        
                                                    </div>                                                    
                                                   
                                                </div>
                                            </div>
                                       </form>
                                      
                                    </div>            
                                </div>
                            </div>
                        </div>
                        <div class="loadingsection">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                        </div>
                        <div class="portlet-body">
                       
                        <div class="table-responsive" id="sample" > 
                            <form id="frm_confrerence_list" name="frm_confrerence_list" method="post" action="conference_list.php">                          
                            <input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $HiddenSearch; ?>">
                            <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                            <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                            <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable" id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                <thead>
                                <tr>
                                    <th class="tbl_center" > Conference  Id </th>
                                    <th class="tbl_center" > Conference  Name </th>
                                    <th class="tbl_center" > Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if ($_SESSION['master'] == 1) {
                                            $children = array($_SESSION['childrens']);
                                            $ids = $_SESSION['loginid'].",".join(',',$children);
                                            $dbQry="select * from customer_conference where customer_id in ($ids) $HiddenSearchCondn";
                                        } else {
                                            $dbQry="select * from customer_conference where customer_id in ($cid) $HiddenSearchCondn";
                                        }
                                        $getResQry      =   $conn->prepare($dbQry);
                                        $getResQry->execute($srchArr);
                                        $getResCnt      =   $getResQry->rowCount();
                                        $getResQry->closeCursor();
                                        if($getResCnt>0){
                                            $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                            $Start=($Page-1)*$RecordsPerPage;
                                            $sno=$Start+1;
                                                
                                            $dbQry.=" limit $Start,$RecordsPerPage";
                                                    
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute($srchArr);
                                            $getResCnt      =   $getResQry->rowCount();
                                        if($getResCnt>0){
                                            $getResRows     =   $getResQry->fetchAll();
                                            $getResQry->closeCursor();
                                            $s=1;
                                                foreach($getResRows as $conference_info){
                                                    $conference_no   =   $conference_info["id"];
                                                    $conference_name =   $conference_info["conference_name"];
                                                    $last_name       =   $conference_info["lastname"];
                                                   
                                                   
                                    ?>
                                   
                                    <?php if($sportname!=""){ $sportname1=$sportname;} else {
                                                $sportname1=$ls;
                                            }?>
                                   
                                    <tr class="odd gradeX">
                                        <td class="tbl_center_td" style="text-align:center"><?php echo $conference_no; ?></td>

                                        <!-- <td class="tbl_center_td">
                                            <a href="javascript:void(0)" class="edit_popup" data-id="<?php  echo base64_encode($conference_no); ?>" data-name="<?php echo $conference_name;?>" data-sport="<?php echo $sportname1; ?>" data-cid="<?php echo $cid;?>"><?php echo $conference_name; ?></a> 
                                        </td> -->
                                        <td class="tbl_center_td">
                                            <?php echo $conference_name; ?>
                                        </td>

                                        <td class="tbl_center_td">
                                            <?php if($sportname!=""){ $sportname=$sportname;} else {
                                                $sportname=$ls;
                                            }?>
                                            <a href="#" id="edit_division" data-id="<?php  echo base64_encode($conference_no); ?>" data-name="<?php echo $conference_name;?>"
                                            data-sport="<?php echo $sportname1; ?>" data-toggle="modal" class="btn btn-xs btn-success edit_popup"  data-cid="<?php echo $cid;?>" customerid="<?php echo $division['id']; ?>"><i class="fa fa-pencil"></i> Edit
                                            </a>
                                    </td>

                                    </tr>
                                    <?php
                                        $s++;
                                       }
                                    } 
                                        else{
                                            // if ($_SESSION['master'] != 1)
                                            // echo "<tr><td colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
                                        }
                                    }
                                    else{
                                        // if ($_SESSION['master'] != 1)
                                            // echo "<tr><td  colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                            <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
                                $FormName = "frm_confrerence_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                           ?>
                        </div>
                        </form>
                    </div>
                    
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->          
            
                            <!-- END SAMPLE TABLE PORTLET-->
            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/manageconference.js" ></script>


