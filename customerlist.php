<?php 
include_once('session_check.php'); 
include_once("connect.php");
if (!empty($_SESSION['loginid']))  {

    if ($_SESSION['usertype'] == 'admin') {
        $uid = $_SESSION['loginid']; 
    } else { 
        header('Location:login.php');
        exit;
    }
} else {
    header('Location:login.php');
    exit;
}

// print_r($_POST);exit;
if(isset($_POST["hdnsearch"])){
    $HiddenSearch = $_POST["hdnsearch"];
    $HiddenSearchText = ( $HiddenSearch )? $HiddenSearch : $_POST['HiddenSearch'] ;
}
$HiddenSearchCondn = !empty($HiddenSearchText)? "and name like '%$HiddenSearchText%'" : "" ;

// $custqry = $conn->prepare("select * from customer_info where user_type=:user_type"); 
// $custqry = $conn->prepare("select * from customer_info where user_type=:user_type $HiddenSearchCondn");
// $QryArr = array(":user_type"=>"user");        
// $custqry->execute($QryArr);
// $QryCntTeams = $custqry->rowCount(); 
// $get_customers = $custqry->fetchAll(PDO::FETCH_ASSOC);

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "New customer has been added successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 2) {
        $alert_message = "Customer has been deleted successfully";
        $alert_class = "alert-danger";
    } else if($_GET['msg'] == 3) {
        $alert_message = "You have selected nothing!, Please select delete option";
        $alert_class = "alert-danger";
    } else if($_GET['msg'] == 4) {
        $alert_message = "Customer has been updated successfully";
        $alert_class = "alert-success";
    } else {
        $alert_message = "Something wrong!!";
        $alert_class = "alert-danger";
    }
}

/****Paging ***/
$Page = 1;$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php');?>
<link href="assets/custom/css/customerlist.css" rel="stylesheet" type="text/css" />
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>            
          
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet light col-md-6 col-sm-6 col-xs-12 portletbg" >
                        <div class="portlet-title ">
                            <div class="col-md-12 removerightpadding">
                                <!-- <div class="caption font-red-sunglo col-md-2 col-sm-2 col-xs-2 searchtext">
                                    <i class="icon-search font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase">Search</span>
                                </div> -->
                                <div class="col-md-12 col-sm-12 col-xs-12 removerightpadding">                                       
                                    <div class="col-md-10 col-sm-10 col-xs-12 removerightpadding">
                                        <div class="form-group ">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-radius" id="organizationsearch" placeholder="Search by organization name" value="<?php echo $HiddenSearch; ?>">
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-md-2 col-sm-2 col-xs-12 searchrightpadding">
                                        <div class="form-group">
                                            <!-- <input type="button" id="searchbtn" class="btn btn-secondary " value="Search" name="search"> -->
                                            <input type="button" id="resetbtn" class="btn btn-danger " value="Reset" name="reset">
                                        </div>
                                    </div>
                               </div>

                            </div>
                        </div>
                    </div>
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="    clear: both;">

                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF CUSTOMERS                       
                            </h3>
                            <div class="pull-right">                        
                                <input type="button" class="btn btn-small addcustomerbtn" onclick="document.location='addcustomer.php'" value="Add customer" style="margin-right:16px;border-radius: 4px !important;">
                            </div>

                        </div>
                        <div class="loadingsection">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                        </div>
                    <div class="customerlistparent">
                        <div class="table-responsive">
                        <form id="customer_list" name="customer_list" method="post" action="">
                        <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                        <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                        <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                        <input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $HiddenSearch; ?>">
                            <!-- <table class="table table-striped table-hover customerlist-tbl" id="sample_5"> -->
                            
                            <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_5" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                <thead>
                                    <tr>
                                        <th nowrap> Customer&nbsp;Id&nbsp; </th>
                                        <th nowrap> Organization&nbsp;Name&nbsp; </th>
                                        <th nowrap> Contact&nbsp;name&nbsp; </th>
                                        <th nowrap> Organization&nbsp;Type </th>
                                        <th nowrap> Website&nbsp;URL </th>
                                        <th nowrap> Scoring&nbsp;platform&nbsp; </th>
                                        <th nowrap> Site&nbsp;template&nbsp; </th>
                                        <th nowrap> Livescoring&nbsp;Template&nbsp; </th>
                                        <th nowrap> Renewal&nbsp;date&nbsp; </th>
                                        <th nowrap> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 

                                // $dbQry = "select * from customer_info where user_type=:user_type";
                                $dbQry = "select * from customer_info where user_type=:user_type $HiddenSearchCondn";
                                $getResQry      =   $conn->prepare($dbQry);
                                $QryArr = array(":user_type"=>"user");
                                $getResQry->execute($QryArr);
                                $getResCnt      =   $getResQry->rowCount();
                                $getResQry->closeCursor();
                                if ($getResCnt > 0) {
                                    $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;                                        
                                    $dbQry.=" limit $Start,$RecordsPerPage"; 
                                    $getResQry      =   $conn->prepare($dbQry);
                                    $getResQry->execute($QryArr);
                                    $getResCnt      =   $getResQry->rowCount();

                                    if($getResCnt>0){
                                        $getResRows     =   $getResQry->fetchAll();
                                        $getResQry->closeCursor();
                                        $s=1;
                                        foreach ($getResRows as $customer) { ?> 
                                            <tr>
                                                <td nowrap><?php echo $customer['id'] ?></td>
                                                <td nowrap><?php echo $customer['name'] ?></td>
                                                <td nowrap><?php echo $customer['contact_name'] ?></td>
                                                <td nowrap>
                                                <?php 
                                                $orgid= $customer['customer_type'];
                                                $orgQry = $conn->prepare("select * from customer_organization_type where id=:orgid");        
                                                $QryArr = array(":orgid"=>$orgid);        
                                                $orgQry->execute($QryArr);                                        
                                                $org_type = $orgQry->fetch(PDO::FETCH_ASSOC);
                                                echo $org_type['name'];
                                                ?></td>
                                                <td nowrap><?php echo $customer['site_url'] ?></td>
                                                <td nowrap><?php echo $customer['scoring_platform'] ?></td>
                                                <td nowrap><?php echo $customer['site_template'] ?></td>
                                                <td nowrap><?php echo $customer['livescoring_template'] ?></td>
                                                <td nowrap><?php echo $customer['renewal_date'] ?></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td nowrap>
                                                            <a href="addcustomer.php?id=<?php echo base64_encode($customer['id']); ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Edit
                                                                
                                                            </a>
                                                            </td>
                                                            <td nowrap>   
                                                            <a href="#myModalDelete" id="openBtn" weburl="<?php echo $customer['site_url']; ?>" customerid="<?php echo $customer['id']; ?>" data-toggle="modal" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete
                                                                
                                                            </a>                                                   
                                                            
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php $s++;}
                                        } else {
                                           echo "<tr><td colspan='10' style='text-align:center;'>No Customer(s) found.</td></tr>";
                                        }
                                } else {
                                    echo "<tr><td colspan='10' style='text-align:center;'>No Customer(s) found.</td></tr>";
                                }?>
                                </tbody>
                            </table>
                            
                                <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='10' valign='middle' class='pagination'>";
                                $FormName = "customer_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                               ?>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/addcustomer.js" ></script>

   