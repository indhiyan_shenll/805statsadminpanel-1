<?php 
include_once('connect.php');
include_once('common_functions.php');
include_once('session_check.php');

if($_SESSION['loginid']=='')  {
	echo '<script>window.location="login.php";</script>';
}

$cid="";
if($_SESSION['loginid']!='')  {
  if($_SESSION['usertype']=='user') {
     $cid=$_SESSION['loginid'];
  }
  else{ 
	  echo '<script>window.location="login.php";</script>';
  }
}

    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$urlArr= explode('=',$actual_link);	

    $sports[]=array();
	$sport_qry_str = "select * from customer_subscribed_sports where customer_id=:cid";
	$get_sport_qry = $conn->prepare($sport_qry_str);
	$get_sport_qry->execute(array(":cid"=>$cid));
	$get_soprts_Count = $get_sport_qry->rowCount();
    if ($get_soprts_Count>0){
    	$getSportsRow=$get_sport_qry->fetch();
		foreach ($getSportsRow as $sportlist)
		{
			$sports[]= $sportlist['sport_id']; 
		}
	}
    
    if(isset($_GET['sport'])){
	   $sportname= $_GET['sport'];
	   $sport_qry_strs = "select * from sports where sport_name like :sportname";
	   $get_sport_qrys = $conn->prepare($sport_qry_strs);
	   $get_sport_qrys->execute(array(":sportname"=>$sportname."%"));
	   $get_sp_Count = $get_sport_qrys->rowCount();
	   if($get_sp_Count>0){
	      $getSportsRow=$get_sport_qry->fetch();
	      $sportid= $getSportsRow['sportcode'];
	    }
    }
    else{
		$sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
		$searchsportid = '4441,4442,4443,4444,4445,4446';
	}


if(isset($_GET['confid'])){
	$conference_id=base64_decode($_GET['confid']);
} else {
  $conference_id="";
}

if ($conference_id!="") {
    $Conference_qry_str = "select * from customer_conference where id=:conference_id";
    $get_conference_qry = $conn->prepare($Conference_qry_str);
    $get_conference_qry->execute(array(":conference_id"=>$conference_id));
    $get_conference_rowCount = $get_conference_qry->rowCount();
	if($get_conference_rowCount>0){
        $getResRow=$get_conference_qry->fetch();
        $conference_name_db   = $getResRow['conference_name'];
        $mode="Edit";
    } else {
		$conference_name_db=   "";
        $mode="Add";
    }
}   

if(isset($_POST["confere_name"])){
	$confere_name 		=	$_POST['confere_name']; 
	if($mode=="Edit"){
		$update_results=array(":cid"=>$cid,":confere_name"=>$confere_name,":id"=>$conference_id);
		$updateQry="update customer_conference set customer_id=:cid,conference_name=:confere_name where id=:id";
		$prepupdateQry=$conn->prepare($updateQry);
		$updateRes=$prepupdateQry->execute($update_results);
		if($updateRes){
			header('Location:conference_list.php?msg=2&sport='.$sportname);
		   exit;
		}
   }
	else{
		$insert_results=array(":cid"=>$cid, ":confere_name"=>$confere_name);
        $insertqry="insert into customer_conference(customer_id, conference_name) values(:cid, :confere_name)";
        $prepinsertqry=$conn->prepare($insertqry);
		$insertRes=$prepinsertqry->execute($insert_results);
        if($insertRes){
		    header('Location:conference_list.php?msg=1&sport='.$sportname);
	        exit;
		}
     }
}

include_once('header.php');
?>
<style>
.form-control{
	border-radius: 4px !important;
}
.error{color: red;}
.btn-danger:hover {
    color: #fff6f6;
    text-shadow: 0px -1px 0px #760000;
}
.btn-danger:hover, .btn-danger:active, .btn-danger.active, .btn-danger.disabled, .btn-danger[disabled], .btn-group.open .btn-danger.dropdown-toggle {
    background-color: #E40304;
}
.btn-danger{
    background-image: -webkit-linear-gradient(top, #E95D5D, #E40304);
    border-color: rgba(0, 0, 0, 0.15);
    border-radius: 4px !important;
}
.green-meadow{
	background-image: -webkit-linear-gradient(top, #80dcc9,#13866f);
    border-color: rgba(0, 0, 0, 0.15);
    border-radius: 4px !important;
}
</style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
            	<form role="form" name="frm_conference" id="frm_conference" method="Post" enctype="multipart/form-data">
            		<input type="hidden" name="hnd_img" id="hnd_img" value="<?php echo  $image_db ?>">
            		<input type="hidden" id="sportid" value="<?php echo $sportid; ?>" name="sportid">
                    <input type="hidden" id="sportn" value="<?php echo $sportname; ?>" name="sportn">
            		<div class="col-md-6 ">
	                    <!-- BEGIN SAMPLE FORM PORTLET-->
	                    <div class="portlet light  team_bio_portlet">
	                        <div class="portlet-title">
	                            <div class="caption font-red-sunglo">
	                                <i class="icon-settings font-red-sunglo"></i>
	                                <span class="caption-subject bold uppercase">Conference Information</span>
	                            </div>
	                        </div>
	                        <div class="portlet-body form"> 
	                            <div class="form-body">
	                                <div class="col-md-12 col-sm-12 col-xs-12">
		                                <div class="form-group  playerinfo_paddingleft">
		                                    <label>Conference Name: <span class="error">*</span></label>
		                                    <input class="form-control input-sm" type="text" name="confere_name" id="confere_name" placeholder="Conference Name" value="<?php echo $conference_name_db;?>" /> 
		                                </div>
		                            </div> 
		                        </div>
                                <div class="" style="text-align: center;">
                                    <button type="submit" class="btn green-meadow">Submit</button>
                                    <button type="button" class="btn red btn-danger" onclick="document.location='conference_list.php?sport=<?php echo $sportname; ?>'">Cancel</button>
                                    
                                </div>
	                           
	                        </div>
	                    </div>
	                </div>
               </form>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->
<?php include_once('footer.php'); ?>


