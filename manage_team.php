<?php 
include_once('session_check.php'); 
include_once('connect.php');
include_once('common_functions.php'); 
include_once('usertype_check.php');

$signin = '';
if (isset($_SESSION['signin'])) {
    $signin = $_SESSION['signin'];
}

$Tid = '';
if (isset($_GET['tid'])) {
    $Tid = base64_decode($_GET['tid']);
}

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 2) {
        $alert_message = "Duplicate game!!";
        $alert_class = "alert-danger";
    } else if($_GET['msg'] == 1) {
        $alert_message = "Team has been updated successfully";
        $alert_class = "alert-success";
    }
}

if (isset($_GET['sport'])) {

    $SportName = $_GET['sport'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
            $SportId = $QrySportVal['sportcode']; 
        }
    }    
}

$GameQryArr = array();
$GameQry = $conn->prepare("select * from teams_info where id=:tid");
$GameQryArr = array(":tid"=>$Tid);
$GameQry->execute($GameQryArr);
$CntGame = $GameQry->rowCount();
$selteamvideo = $conn->prepare("select * from customer_tv_station where team_id='$Tid' and customer_id='$Cid'");
$selteamvideo->execute();
$FetchVideoRow = $selteamvideo->fetch(PDO::FETCH_ASSOC);

// $fetchvideo = mysql_fetch_array($selteamvideo);
// $tv_station_id = $fetchvideo['active'];
$page_prg = '';
if ($CntGame > 0) {

    $GameRes = $GameQry->fetch(PDO::FETCH_ASSOC);
    $customer_id = $GameRes['customer_id'];
    $sportid = $GameRes['sport_id'];
    $team_email = $GameRes['email'];
    $team_password = $GameRes['password'];
    $page_prg = $GameRes['page_privileges'];
    $all_login = $GameRes['allow_login'];
    $archive_value = $GameRes['archive'];    

    $TeamNameDB = $GameRes['team_name'];
    $PrintNameDB = $GameRes['print_name'];
    $NickNameDB = $GameRes['nick_name'];
    $AbbrevationDB = $GameRes['abbrevation'];
    $AddressDB = $GameRes['address'];    
    $CityDB = $GameRes['city'];
    $StateDB = $GameRes['state'];
    $ZipDB = $GameRes['zip'];
    $StadiumDB = $GameRes['stadium'];
    $CoachDB = $GameRes['coach'];
    $PhoneDB = $GameRes['Cphone'];
    $CEmailDB = $GameRes['CEmail'];
    $EmailDB = $GameRes['email'];
    $PasswordDB = $GameRes['password'];
    $DivisionDB = $GameRes['division'];
    $ConferenceDB = $GameRes['conference'];
    $ArchiveDB = $GameRes['archive'];
    $SuspendedDB = $GameRes['isSuspended'];
    $page_privileage_value = explode("#",$GameRes['page_privileges']); 
    $AlowLoginDB = $GameRes['allow_login'];  
    $TeamImageDB =  $GameRes['team_image'];
    $mode="Edit";    

    $teamvideocustomer = $conn->prepare("select * from customer_tv_station where team_id='$Tid' and customer_id='$customer_id'");
    $teamvideocustomer->execute();
    $Cntteamvideo = $teamvideocustomer->rowCount();
    if ($Cntteamvideo > 0) {
        $videorows = $teamvideocustomer->fetchAll(PDO::FETCH_ASSOC);
        foreach ($videorows as $videocust) {
            $manage_video = $videocust['active'];
        }
    }

    $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
    $sportnquery->execute();
    $Cntsportnquery = $sportnquery->rowCount();
    if ($Cntsportnquery > 0) {
        $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);

        foreach ($sportrows as $sportnames) {
            $SportName = $sportnames['sport_name']; 
            $SportName = strtolower($SportName);
        }
    } else {
        $SportName = "";
    }

} else {

    $TeamNameDB = "";
    $PrintNameDB = "";
    $NickNameDB = "";
    $AbbrevationDB = "";
    $AddressDB = "";
    $CityDB = "";
    $StateDB = "";
    $ZipDB = "";
    $StadiumDB = "";
    $CoachDB = "";
    $PhoneDB = "";
    $CEmailDB = '';
    $EmailDB = "";
    $PasswordDB = '';
    $DivisionDB = '';
    $ConferenceDB = '';
    $ArchiveDB = '';
    $SuspendedDB = '';
    $page_privileage_value = array();
    $AlowLoginDB = '';
    $TeamImageDB = '';
    $mode="Add";
}


$rulelist = '';
$GresArr = '';
$insetArr = '';
$allow_login = '';

if (isset($_POST['addsubmit']) || isset($_POST['resendmail'])) {

    $teamname = $_POST['teamname'];
    $printname = $_POST['printname'];
    $nickname = $_POST['nickname'];
    $abbr = $_POST['abbr'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $venue  = $_POST['venue'];
    $cname = $_POST['cname'];
    $cphone = $_POST['cphone'];
    $cemail = $_POST['cemail'];
    $DivisionList = $_POST['divisionlist'];
    $ConferenceList = $_POST['conferencelist'];
    $rulelist = isset($_POST['rulelist']) ? $_POST['rulelist']: "" ;
    $archive = !empty($_POST['archive']) ? $_POST['archive'] : '0' ;
    $suspended = !empty($_POST['suspended']) ? $_POST['suspended'] : '0' ;
    $manage_videos = $_POST['manage_videos'];
    $allow_login = $_POST['allow_login'];
    $page = '';
    if ($signin == 'team_manager') {
        $allow_login=$all_login ;   
        $email = $team_email;
        $password =$team_password;
        $page= $page_prg;
        $manage_videos=$manage_video;
        $archive=$archive_value;
    } else if ($signin != 'team_manager') {
        if ($allow_login == true) {
            $email= $_POST['login_email'];
            $password= $_POST['login_password'];
            if (!empty($_POST['page_privileage'])) {
                $page=implode("#",$_POST['page_privileage']);
            }
        } else {
            $email = '';
            $password = '';
            $page= '';
        }
    }

    if ($rulelist!="") {
        $DivisionList = $DivisionList ." - ".$rulelist;
    } else {
        $DivisionList = $DivisionList;
    }

    if ($DivisionList !="" ) {
        $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
        $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
        $CntDivisionQuery = $DivisionQuery->rowCount();
        if ($CntDivisionQuery > 0) {
            $DivRows = $DivisionQuery->fetchAll(PDO::FETCH_ASSOC);
            foreach ($DivRows as $Drow) {
                $Divid = $Drow['id'];
            }            
        } else {
            $InsertDivQuery = $conn->prepare("INSERT INTO customer_division(name,custid) VALUES (:divisionlist,:cid)");
            $InsertDivQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
            $Divid = $conn->lastInsertId();
        }
    } else {
        $Divid = "";
    }    

    if ($_SESSION['master'] != 1) {
        if ($ConferenceList !="" ) {
            $ConfQuery = $conn->prepare("select * from customer_conference where id =:conflist OR (conference_name=:conflist and customer_id=:cid)");
            $ConfQuery->execute(array(":conflist"=>$ConferenceList, ":cid"=>$Cid));
            $CntConfQuery = $ConfQuery->rowCount();
            if ($CntConfQuery > 0) {
                $ConfRows = $ConfQuery->fetchAll(PDO::FETCH_ASSOC);
                foreach ($ConfRows as $Crow) {
                    $Confid = $Crow['id'];
                }            
            } else {
                $InsertConfQuery = $conn->prepare("INSERT INTO customer_conference(conference_name,customer_id) VALUES (:conflist,:cid)");
                $InsertConfQuery->execute(array(":conflist"=>$ConferenceList, ":cid"=>$Cid));
                $Confid = $conn->lastInsertId();
            }
        } else {
            $Confid = "";
        }
    }

    $filename = "";
    if (isset($_FILES["file"]["type"])) {
        $validextensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES["file"]["name"]);
        $file_extension = end($temporary);
        if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
        ) && ($_FILES["file"]["size"] < 3000000)//Approx. 300kb files can be uploaded.
        && in_array($file_extension, $validextensions)) {
            if ($_FILES["file"]["error"] > 0) {
                echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
            } else {
                if (file_exists("uploads/teams/" . $_FILES["file"]["name"])) {
                    echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
                } else {
                    $n=date("ymdihs");
                    $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                    $targetPath = preg_replace('/\s+/', '',"uploads/teams/".$n.$_FILES['file']['name']); // Target path where file is to be stored
                    move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                    $filename=preg_replace('/\s+/', '',$n.$_FILES['file']['name']);

                    $cropfilename = $_FILES["file"]["name"];
                    $data = array(
                    'width' => "200",
                    'height' =>"200",
                    'image_source' =>SYSTEM_ROOT_PATH."/".$targetPath,
                    'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/teams/thumb/",
                    'image_name' =>$n.$cropfilename

                    );
                    $cropsucessfully = cropImage($data);
                   
                }
            }
        } 
        // else {
        //     echo '<script type="text/javascript">alert("Invalid file Size or Type");return false;</script>';
        // }
    }


    if ($mode == "Add") {            

        $InsertGame = $conn->prepare("INSERT INTO teams_info(customer_id, team_name, print_name, nick_name, abbrevation, address, state, city, zip, stadium, coach, Cphone, CEmail, conference, division, team_image, sport_id,email,password,allow_login,page_privileges,archive,isSuspended) VALUES ('$Cid', '$teamname', '$printname', '$nickname', '$abbr', '$address', '$state', '$city', '$zip', '$venue', '$cname', '$cphone', '$cemail', '$Confid', '$Divid', '$filename', '$SportId','$email', '$password', '$allow_login','$page','$archive','$suspended')");
        $InsertGame->execute();
        $last_insert_id = $conn->lastInsertId();
        $insertteamvideo = $conn->prepare("INSERT INTO customer_tv_station(team_id, customer_id, active) VALUES ('$last_insert_id', '$Cid', '$manage_videos')");
        $insertteamvideo->execute();

        /*email sent */
        if (!empty($email) && !empty($password)) {

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: support@805stats.com';
            $message='Hello Team,<br/><br/>Please use below details to access the Team Manager Portal.<br/><br/>http://805stats.com/panelv2/teamlogin.php<br/>Email :'.$email."<br/>Password:".$password."<br/><br/><br/>Regards<br/>805Stats Support Team";
            mail($email,'Password', $message, $headers);
        }

        $tid = $conn->lastInsertId();
        // $tid = mysql_insert_id();
        $InsertQry = $conn->prepare("insert into player_info set lastname='TEAM',firstname='A',position='G',uniform_no='TM',customer_id='$cid',team_id='$tid',sport_id='$sportid'");
        $InsertQry->execute();
        $InsertQry = $conn->prepare("insert into team_stats set teamcode='$tid', customer_id='$Cid', year='date(Y)'");
        $InsertQry->execute();            
        // }  
        if ($InsertGame) {
            header('Location:team_list.php?msg=1');
            exit;
        }
    } else {
        
        if(($filename=="") or ($filename==$n)){
            $filename = isset($_POST['oldfilename'])? $_POST['oldfilename'] : "";
        }

        // echo "update teams_info set customer_id='$Cid', team_name='$teamname', nick_name='$nickname', print_name='$printname', abbrevation='$abbr', address='$address', state='$state', city='$city', stadium='$venue', coach='$cname', Cphone='$cphone', CEmail='$cemail',conference='$Confid', division='$Divid', team_image='$filename', sport_id='$sportid' ,archive='$archive', email='$email', password='$password', allow_login='$allow_login',page_privileges='$page' where id='$Tid'";exit;
        $updateteam= $conn->prepare("update teams_info set  team_name='$teamname', nick_name='$nickname', print_name='$printname', abbrevation='$abbr', address='$address', state='$state', city='$city', stadium='$venue', coach='$cname', Cphone='$cphone', CEmail='$cemail',conference='$Confid', division='$Divid', team_image='$filename', sport_id='$sportid' ,archive='$archive', email='$email', password='$password', allow_login='$allow_login',page_privileges='$page', isSuspended='$suspended' where id='$Tid'");
		//customer_id='$Cid',
        $updateteam->execute();
        $selteamvideo = $conn->prepare("select * from customer_tv_station where team_id='$Tid' and customer_id='$Cid'");
        $selteamvideo->execute();
        $Cntselteamvideo = $selteamvideo->rowCount();
        if ($Cntselteamvideo  > 0) {
            $fetchvideo = $selteamvideo->fetch(PDO::FETCH_ASSOC);

            $tv_station_id = $fetchvideo['id'];
            $updateteamvideo= $conn->prepare("update customer_tv_station set active='$manage_videos' where team_id='$Tid'");
            $updateteamvideo->execute();
        } else {
        $insertteamvideo = $conn->prepare("INSERT INTO customer_tv_station(team_id, customer_id, active) VALUES ('$Tid', '$Cid', '$manage_videos')");
        $insertteamvideo->execute();
        }   

        $mailstatus = '';
        if (isset($_POST['resendmail']) ) {

            $tid = base64_decode($_GET['tid']);
            $allow_login= $_POST['allow_login'];
            if ($allow_login == true) {
                $email= $_POST['login_email'];
                $password= $_POST['login_password'];
            } else {
                $email = '';
                $password = '';
            }
            
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: support@805stats.com';
            $message='Hello Team,<br/><br/>Please use below details to access the Team Manager Portal.<br/><br/>http://805stats.com/panelv2/teamlogin.php<br/>Email :'.$email."<br/>Password:".$password."<br/><br/><br/>Regards<br/>805Stats Support Team";
            $mailstatus = mail($email,'Password', $message, $headers);
            $updateteam= $conn->prepare("update teams_info set customer_id='$Cid', email='$email', password='$password', allow_login='$allow_login' where id='$tid'");
            $updateteam->execute();
        }


        if ($_SESSION['usertype'] == 'team_manager') {

            header('Location:manage_team.php?msg=1&tid='.base64_encode($Tid));
            exit;             
        } else {
            // if($updateteam){
                header('Location:team_list.php?msg=2');
                exit;
            // }
        }
        
    }
}



include_once('header.php'); ?>
<link href="assets/custom/css/manageteam.css" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<input type="hidden" id="sportid" value="<?php echo $SportId; ?>" name="sportid">
<input type="hidden" id="sportn" value="<?php echo $SportName; ?>" name="sportn">
<input type="hidden" id="teamid" value="<?php echo $Tid; ?>" >
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            
            <!-- END PAGE HEADER-->
            <div class="row">
                <form id="teamform" method="POST"  enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="col-md-12 left-right-padding">
                            <?php if (isset($_GET['msg'])) { ?>
                            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <p> <?php echo $alert_message; ?> </p>
                            </div>
                            <?php } ?>
                            <div class="portlet light info-caption">

                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo caption-width-team">
                                        <i class="icon-settings font-red-sunglo" style="margin-top:8px;"></i>
                                        <span class="caption-subject bold uppercase" style="vertical-align: sub;"> Team Information</span>
                                        <p class="mode-color"><?php echo $mode;?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 left-padding">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <div class="form-body bottom-padding mangeteam-form-bottom">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="teamname">Team Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="teamname" name="teamname" placeholder="" maxlength="25" value="<?php echo $TeamNameDB; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="printname">Print Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="printname" name="printname" placeholder="" maxlength="25" value="<?php echo $PrintNameDB; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nickname">Nick Name
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="nickname" name="nickname" placeholder="" value="<?php echo $NickNameDB; ?>" maxlength="25"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="abbr">Abbreviation
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="abbr" name="abbr" placeholder="" value="<?php echo $AbbrevationDB; ?>" maxlength="5" > 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address">Address
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="address" name="address" placeholder="" value="<?php echo $AddressDB; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" value="<?php echo $CityDB; ?>" maxlength="25"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6"> -->
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label for="state">State</label>
                                                        <input type="text" class="form-control input-sm" name="state" id="state" maxlength="2"placeholder="" value="<?php echo $StateDB; ?>" >    
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group  ">
                                                        <label for="zip">Zip</label>      
                                                        <input type="text" class="form-control input-sm" name="zip" id="zip" placeholder="" value="<?php echo $ZipDB; ?>" maxlength="5">          
                                                    </div>
                                                </div>
                                            <!-- </div> -->
                                            <!-- </div> -->
                                            <!-- <div class="col-md-6">
                                                
                                            </div> -->
                                            <?php if ($_SESSION['master'] != 1) {
                                                // echo "</div>";
                                            ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="conferencelist">Conference
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <select class="form-control input-sm" name="conferencelist" id="conferencelist">
                                                        <option value="">---Select---</option>
                                                        <option value="addnew">Add New</option>
                                                        <?php 
                                                        $FetchConf = json_decode(getCustomerConference($Cid),true);
                                                        foreach ($FetchConf as $FetchRows) { ?>
                                                            <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $ConferenceDB ? "selected" :"" ?> ><?php echo $FetchRows['conference_name']; ?></option> 
                                                        <?php } ?>             
                                                    </select>  
                                                </div>
                                            </div>
                                            <div class="modal fade " id="ConferenceModal" role="dialog">
                                                <div class="modal-dialog">               
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title" id="">Add new conference</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group ">    
                                                                <label for="addconftext">Enter New Conference</label>
                                                                <input type="text" id="addconftext" maxlength="10" class="form-control " name="addconftext" />             
                                                            </div>
                                                            <input type="button" name="addconfbtn" class="btn btn-primary" value="Submit" id="addconfbtn">
                                                            <button class="btn default cancelbtn" type="button" data-dismiss="modal">Cancel</button>      
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } else {echo "</div>";}?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="divisionlist">Division
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <?php 
                                                    if ($_SESSION['master'] != 1) { ?>
                                                    <select class="form-control input-sm" name="divisionlist" id="divisionlist">
                                                        <option value="">---Select---</option>
                                                        <option value="addnew">Add New</option>
                                                        <?php 
                                                        $FetchConf = json_decode(getCustomerDivisions($Cid),true);
                                                        foreach ($FetchConf as $FetchRows) { ?>
                                                            <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $DivisionDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php } ?>             
                                                    </select>
                                                    <?php } else {?> 
                                                    <select class="form-control input-sm" name="divisionlist" id="divisionlist">
                                                        <option value="">---Select---</option>
                                                        <?php 
                                                        $children = array($_SESSION['childrens']);
                                                        $ids = join(',',$children); 
                                                        $divlist = $conn->prepare("select * from customer_division where custid in ($ids) or custid='$Cid'");
                                                        $divlist->execute();
                                                        $Cntdivlist = $divlist->rowCount();
                                                        if ($Cntdivlist > 0) {
                                                            $FetchDiv = $divlist->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($FetchDiv as $FetchRows) { ?>
                                                                <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $DivisionDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php }} ?>             
                                                    </select>
                                                    <?php } ?> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rulelist">Division Rule
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <select class="form-control input-sm" name="rulelist" id="rulelist">
                                                        <option value="">---Select---</option>
                                                        <?php if ($SportName == 'basketball')
                                                        { ?>
                                                        <option value="FIBA">FIBA</option>
                                                        <option value="HS">HS</option>
                                                        <option value="NBA">NBA</option>
                                                        <option value="NCAA">NCAA</option>
                                                        <option value="NCAAW">NCAAW</option>
                                                        <?php } ?>

                                                        <?php if($SportName == 'baseball' || $SportName == 'softball')
                                                        {?>
                                                            <option value="HS_BA">HS_BA</option>
                                                            <option value="HS_SB">HS_SB</option>
                                                            <option value="MLB">MLB</option>
                                                            <option value="NCAA_BA">NCAA_BA</option>
                                                            <option value="NCAA_SB">NCAA_SB</option>
                                                        <?php } ?>

                                                        <?php if($SportName == 'football')
                                                        { ?>
                                                            <option value="Arena">Arena</option>
                                                            <option value="HS">HS</option>
                                                            <option value="Indoor">Indoor</option>
                                                            <option value="NCAA">NCAA</option>
                                                            <option value="NFL">NFL</option>
                                                        <?php } ?>            
                                                    </select>  
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Division Modal -->
                                        <div class="modal fade " id="DivisionModal" role="dialog">
                                            <div class="modal-dialog">                                                    
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Add new division</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">    
                                                            <label for="adddivtext">Enter New Division</label>
                                                            <input type="text" id="adddivtext" class="form-control" name="adddivtext"  maxlength="10"/>
                                                        </div>
                                                        <input type="button" name="adddivbtn" class="btn btn-primary" value="Submit" id="adddivbtn">
                                                        <button class="btn default cancelbtn" type="button" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="venue">Venue(Gym, Stadium etc)
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="venue" name="venue" placeholder="" value="<?php echo $StadiumDB; ?>" maxlength="25"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cname">Coach Name
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="cname" name="cname" placeholder="" value="<?php echo $CoachDB; ?>"  maxlength="25"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cphone">Phone
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="cphone" name="cphone" placeholder="" value="<?php echo $PhoneDB; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cemail">Coach Email
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="cemail" name="cemail" placeholder="" value="<?php echo $CEmailDB; ?>" > 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions form-actions-btn custom-form-action">

                                            <button type="submit" class="btn customgreenbtn" name="addsubmit"><?php echo $mode == "Add" ? "Save" : "Update" ?></button>
                                            <?php if ($mode == "Edit") { ?>
                                            <button type="submit" class="btn customgreenbtn" name="resendmail" id="resendmail">Resend Mail</button>
                                            <?php } ?>
											<?php if ($_SESSION['usertype'] != 'team_manager') {?>
                                            <button type="button" class="btn customredbtn" id="cancelbtn">Cancel</button>
											<?php }?>
                                        </div>                                                                                
                                    </div>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-md-6 right-padding">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <div class="form-body bottom-padding mangeteam-form-bottom">
                                    <?php if ($signin != 'team_manager') { ?>
                                        <div class="row">
                                            
                                            <?php 
                                            if ($AlowLoginDB == true) { 
                                                $AllowTeamLoginStyle = "display:block";
                                            } else {
                                                $AllowTeamLoginStyle = "display:none";
                                            }                                            
                                            ?>
                                            <div class="col-md-6">
                                                <div class="form-group bottommargin">
                                                    <label>Allow team login</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio"> Yes
                                                            <input type="radio" value="1" name="allow_login" id="teamlogin-yes" <?php echo ($AlowLoginDB == true)?'checked':'' ?> >
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio"> No
                                                            <input type="radio" value="0" name="allow_login" id="teamlogin-no"  <?php echo ($AlowLoginDB == false)?'checked':'' ?>>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group bottommargin">
                                                    <label>Manage Videos</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio"> Yes
                                                            <input type="radio" value="1" name="manage_videos" id="manage_videos_yes" <?php echo ($FetchVideoRow['active'] == true)?'checked':'' ?> >
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio"> No
                                                            <input type="radio" value="0" name="manage_videos" id="manage_videos_no" <?php echo ($FetchVideoRow['active'] == false)?'checked':'' ?>>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="allow-team-login-edit" style="<?php echo $AllowTeamLoginStyle; ?>">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="login_email">Email
                                                        </label>
                                                        <input type="text" class="form-control input-sm" id="login_email" name="login_email" placeholder="" value="<?php echo $EmailDB; ?>" > 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="login_password">Password
                                                        </label>
                                                        <input type="text" class="form-control input-sm" id="login_password" name="login_password" placeholder="" value="<?php echo $PasswordDB; ?>" maxlength="25"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group bottommargin">
                                                        <label>Page Privilege</label>
                                                        <div class="mt-checkbox-inline">
                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" id="page_privileage1" name="page_privileage[]" value="1" <?php echo (in_array('1',$page_privileage_value))?'checked':'' ?>> Edit Team
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" id="page_privileage2" name="page_privileage[]" value="2" <?php echo (in_array('2',$page_privileage_value))?'checked':'' ?>> Games
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" id="page_privileage3" name="page_privileage[]" value="3" <?php echo (in_array('3',$page_privileage_value))?'checked':'' ?>> Players
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                        }?>
                                        <?php if($signin != 'team_manager'){?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Team archive</label>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox"> Archive
                                                            <input type="checkbox" id="archive" name="archive" value='1' <?php echo ($ArchiveDB == '1')?'checked':'' ?> >
                                                            <span></span>
                                                        </label>                                                        
                                                    </div>
                                                </div>
                                            </div>         
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Suspend Team</label>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox"> Suspend
                                                            <input type="checkbox" id="suspended" name="suspended" value='1' <?php echo ($SuspendedDB == '1')?'checked':'' ?> >
                                                                <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                           } ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="login_email">
													Team logo(<span class="teamlogospan">No larger than 300kb </span>)
													<span class="required"> * </span>
                                                    </label>

                                                    <input type="file" id="file" class="add_uploadimg" name="file" onchange="showimagepreview(this); document.getElementById('rmv').style.display='block';">
                                                </div>
                                            </div>
                                            <?php 
                                            if ($mode == "Add") { ?>
                                            <div class="col-md-6">
                                                <div class="form-group">                                                     
                                                    <a class="btn customredbtn removebtn" id="rmv" onclick="removeimage()">Remove Image</a>
                                                </div>
                                            </div>
                                            <?php } if ($signin == 'team_manager'){?>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Team archive</label>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox"> Archive
                                                            <input type="checkbox" id="archive" name="archive" value='1' <?php echo ($ArchiveDB == '1')?'checked':'' ?> >
                                                            <span></span>
                                                        </label>                                                        
                                                    </div>
                                                </div>
                                            </div>         
                                            <?php }?>
                                                                              
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Team archive</label>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox"> Archive
                                                            <input type="checkbox" id="archive" name="archive" value='1' <?php echo ($ArchiveDB == '1')?'checked':'' ?> >
                                                            <span></span>
                                                        </label>                                                        
                                                    </div>
                                                </div>
                                            </div>         
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Suspended Team</label>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox"> Suspended
                                                            <input type="checkbox" id="suspended" name="suspended" value='1' <?php echo ($SuspendedDB == '1')?'checked':'' ?> >
                                                                <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <?php if($TeamImageDB !=""){ ?>
                                                      <img id="pwv" class="imgprvw" src="uploads/teams/<?php echo $GameRes['team_image']; ?>" alt="" style="width:96px;height:96px;" />
                                                      <input type="hidden" class="form-control" name="oldfilename" id="oldfilename" value="<?php echo $GameRes['team_image']; ?>">
                                                <?php } else { ?>
                                                        <img id="pwv" class="imgprvw" alt="Preview Image" src="images/add.png" style="width:96px;height:96px;">
                                                <?php
                                                } ?>
                                                </div>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>                    
                        </div>                        
                    </div>                    
                </form>
            </div>            
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>

<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="assets/custom/js/addteam.js" type="text/javascript" ></script>
<?php 
if ($mode == "Edit") { ?>
<script>
    $( document ).ready(function() {
      $( "#file" ).rules( "remove" );
    });
</script>
<?php }
?>
