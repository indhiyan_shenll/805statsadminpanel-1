<?php
include_once('session_check.php');
include_once('connect.php'); 
include_once('common_functions.php');


$basketball_array = array("G"=>"G","C"=>"C","F"=>"F","SG"=>"SG","GF"=>"GF","PG"=>"PG","PF"=>"PF","SF"=>"SF");

$baseball_softball_array = array("Extra fldr"=>"Extra fldr","Pitcher"=>"Pitcher","Catcher"=>"Catcher","First Base"=>"First Base","Second Base"=>"Second Base","Third Base"=>"Third Base","Shortstop"=>"Shortstop","Left Field"=>"Left Field","Center Field"=>"Center Field", "Right Field"=>"Right Field", "Des Hitter"=>"Des Hitter","Des Player"=>"Des Player","Pinch Hit"=>"Pinch Hit", "Pinch Run"=>"Pinch Run", "Infielder"=>"Infielder", "Outfielder"=>"Outfielder", "Utility"=>"Utility");

$soccer_array = array("D"=>"D","F"=>"F","G"=>"G","MF"=>"MF");

$volleyball_array = array("DS"=>"DS","LIBERO"=>"LIBERO","LSH"=>"LSH","MB"=>"MB","MH"=>"MH","OH"=>"OH","OPPOSITE"=>"OPPOSITE","PASSER"=>"PASSER","RSH"=>"RSH","SETTER"=>"SETTER");

$football_array=array("C"=>"C","C/G"=>"C/G","CB"=>"CB","DB"=>"DB","DE"=>"DE","DL"=>"DL","DT"=>"DT","FB"=>"FB","FS"=>"FS","G"=>"G","G/T"=>"G/T","K"=>"K","KR"=>"KR","LB"=>"LB","NT"=>"NT","OL"=>"OL", "L"=>"L","PR"=>"PR","QB"=>"QB","RB"=>"RB","S"=>"S","SS"=>"SS","T"=>"T","TE"=>"TE","WR"=>"WR");

if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == "updateswitch"){
  $player_id=$_REQUEST['PlayerId'];

   $QryExeDiv = $conn->prepare("select * from player_info as player INNER JOIN sports as sport ON player.sport_id=sport.sportcode where player.id=:player_id");
	$QryarrCon = array(":player_id"=>$player_id);
    $QryExeDiv->execute($QryarrCon);
	$QryCntPlayerconf = $QryExeDiv->rowCount();
	if($QryCntPlayerconf>0){
        $getRowRes=$QryExeDiv->fetchAll();

        foreach ($getRowRes as $Player_data) {
        	$uniform_no=$Player_data["uniform_no"];
        	$position=$Player_data["position"];
        	$sportname=$Player_data["sport_name"];
            $image=$Player_data["image"];
            
        	$div_forms=$uniform_no;
        	$div_form1="<option value=''>Select Position</option>";

        	if($sportname=="basketball"){
                foreach ($basketball_array as $key => $value) {
                    $selected =(strtolower($key)==strtolower($position))?"selected":"";
                    $div_form1.="<option value='".$key."' ".$selected.">".$value."</option>";
                }
        	}

            if($sportname=='baseball' || $sportname=='softball'){
                foreach ($baseball_softball_array as $key => $value) {
                    $selected =(strtolower($key)==strtolower($position))?"selected":"";
                    $div_form1.="<option value='".$key."' ".$selected.">".$value."</option>";
                }
            }

            if($sportname=='soccer'){
                foreach ($soccer_array as $key => $value) {
                    $selected =(strtolower($key)==strtolower($position))?"selected":"";
                    $div_form1.="<option value='".$key."' ".$selected.">".$value."</option>";
                }
            }

            if($sportname=='volleyball'){
                foreach ($volleyball_array as $key => $value) {
                    $selected =(strtolower($key)==strtolower($position))?"selected":"";
                    $div_form1.="<option value='".$key."' ".$selected.">".$value."</option>";
                }
            }

            if($sportname=='football'){
                foreach ($football_array as $key => $value) {
                    $selected =(strtolower($key)==strtolower($position))?"selected":"";
                    $div_form1.="<option value='".$key."' ".$selected.">".$value."</option>";
                }
            }


        }

       echo $div_form="Success"."##^^##".$div_forms."##^^##".$div_form1."##^^##".$position."##^^##".$image;
       exit;
    } else {
    	echo "failure"; 
    	exit;
    }
}

if(isset($_POST['post_type']) && $_POST['post_type'] == "manage_upadte_switch"){

    $info_file      = $_REQUEST['info_file'];
    $player_id      = $_REQUEST['player_id'];
    $params             =   array();
    parse_str($_POST["form_data"], $params);
        $uniform_no =  $params["uniform_no"];
        $position   =  $params["position"];
        $sel_switch =  $params["sel_switch"]; 
        $isactive   =  $params["isactive"];
        $teamid     =  $params["hnd_teamid"];
        $seasionid  =  $params["hnd_seasionid"];
        $divisionid =  $params["hnd_divisionid"];
        $conferenceid = $params["hnd_conferenceid"];
        $cid=$_SESSION["loginid"];
         
         $upload_file_db =  $params["hnd_image"];
          if(!empty($_FILES["info_file"]["name"])){
            $extension = pathinfo($_FILES["info_file"]["name"],PATHINFO_EXTENSION);
            $upfilename=md5(time()).'.'.$extension; 
            move_uploaded_file($_FILES["info_file"]["tmp_name"],SYSTEM_ROOT_PATH."/uploads/players/".$upfilename);
            $data = array(
             'width' => "200",
             'height' =>"200",
             'image_source' =>SYSTEM_ROOT_PATH."/uploads/players/".$upfilename,
             'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/players/thumb/",
             'image_name' =>$upfilename
           );     
           $cropsucessfully = cropImage($data);
        } else {
           $upfilename=$upload_file_db;
        }
        $update_results=array(":uniform_no"=>$uniform_no,":position"=>$position ,":imageuploads"=>$upfilename,":status"=>$isactive, ":id"=>$player_id);
        $updateQry="update player_info set  uniform_no=:uniform_no, position=:position, isActive=:status, image=:imageuploads where id=:id";
        $prepupdateQry=$conn->prepare($updateQry);
        $updateRes=$prepupdateQry->execute($update_results);

        if($teamid!="" && $seasionid!="" && $divisionid!="" && $conferenceid!=""){

            $update_result=array(":cid"=>$cid, ":team_id"=>$teamid,":season_id"=>$seasionid, ":division_id"=>$divisionid, ":conference_id"=>$conferenceid, ":status"=>$isactive, ":id"=>$player_id);
            $updateTeamQry="update customer_team_player set customer_id=:cid, team_id=:team_id, season_id=:season_id, division_id=:division_id, conference_id=:conference_id, status=:status where player_id=:id";
            $prepupdateTeamQry=$conn->prepare($updateTeamQry);
            $updateResult=$prepupdateTeamQry->execute($update_result);
        }
        if($updateResult){
			$src = 'uploads/players/thumb/'.$upfilename;
			$PlayerImg   = "<img src='$src' style='height:40px;width:40px;'>";

           echo "success###".$PlayerImg."###".$player_id."###".$isactive;
           exit;
        } else {
             echo "failure";
           exit;
        }
}
?>